#include "TermProject.h"

vector<double> getDiagonalConditioningInverseVector(SparseMatrix matrix, const vector<double>& q) {
    vector<double> res = vector<double>(q);
    for (uint32_t cnt = 0; cnt < q.size(); ++cnt) {
        res[cnt] /= matrix.getEl(cnt, cnt);
    }
    return res;
}

vector<double> addTwoVectors(const vector<double>& v1, const vector<double>& v2) {
    if (v1.size() != v2.size()) {
        std::cerr << "Vectors dimensions: " << v1.size() << " and " << v2.size() << " do not align" << endl;
        raise(SIGSEGV);
    }
    vector<double> res = vector<double>(v1);
    for (uint32_t cnt = 0; cnt < v1.size(); ++cnt) {
        res[cnt] += v2[cnt];
    }
    return res;
}

vector<double> vectorMultiplyOnDouble(const vector<double>& v1, double c) {
    vector<double> res = vector<double>(v1);
    for (uint32_t cnt = 0; cnt < v1.size(); ++cnt) {
        res[cnt] *= c;
    }
    return res;
}

double vectorsScalarProduct(const vector<double>& v1, const vector<double>& v2) {
    if (v1.size() != v2.size()) {
        std::cerr << "Vectors dimensions: " << v1.size() << " and " << v2.size() << " do not align" << endl;
        raise(SIGSEGV);
    }
    double res = 0;
    for (uint32_t cnt = 0; cnt < v1.size(); ++cnt) {
        res += v1[cnt] * v2[cnt];
    }
    return res;
}

double vectorDistance(const vector<double>& v1, const vector<double>& v2) {
    if (v1.size() != v2.size()) {
        std::cerr << "Vectors dimensions: " << v1.size() << " and " << v2.size() << " do not align" << endl;
        raise(SIGSEGV);
    }
    double res = 0;
    for (uint32_t cnt = 0; cnt < v1.size(); ++cnt) {
        res += abs(v1[cnt] * v1[cnt] - v2[cnt] * v2[cnt]);
    }
    return res;
}

double vectorAbs(const vector<double>& v1) {
    double res = 0;
    for (auto& el: v1) {
        res += el * el;
    }
    return std::sqrt(res);
}

vector<double> solveSystemMCGDF(SparseMatrix matrix, const vector<double>& rVector, double eps, uint32_t maxIter) {
    if (matrix.getSize() != rVector.size()) {
        std::cerr << "Matrix dimensions: " << matrix.getSize() << " and vector size: " << rVector.size() << " do not align" << endl;
        raise(SIGSEGV);
    }
    vector<double> xkm1 = vector<double>(rVector.size(),  random()/RAND_MAX);
    vector<double> rkm1 = vector<double>(addTwoVectors(rVector, vectorMultiplyOnDouble(matrix * xkm1, -1)));
    vector<double> zkm1 = getDiagonalConditioningInverseVector(matrix, rkm1);
    double alphak = 0;
    vector<double> xk = xkm1;
    vector<double> rk = rkm1;
    vector<double> zk = zkm1;
    double betak = 0;
    double acc = 0.007;
    for (uint32_t cnt = 0; cnt < maxIter; ++cnt) {
        alphak = acc * (vectorsScalarProduct(getDiagonalConditioningInverseVector(matrix, rkm1), rkm1))
                 / (vectorsScalarProduct(matrix * zkm1, zkm1));
        xk = addTwoVectors(xkm1, vectorMultiplyOnDouble(zkm1, alphak));
        rk = addTwoVectors(rkm1, vectorMultiplyOnDouble(matrix * zkm1, -alphak));
        betak = acc * (vectorsScalarProduct(getDiagonalConditioningInverseVector(matrix, rk), rk))
                 / (vectorsScalarProduct(getDiagonalConditioningInverseVector(matrix, rkm1), rkm1));
        zk = addTwoVectors(getDiagonalConditioningInverseVector(matrix, rk), vectorMultiplyOnDouble(zkm1, betak));
        xkm1 = xk;
        rkm1 = rk;
        zkm1 = zk;
        if (vectorAbs(rk) / vectorAbs(rVector) < eps) {
            return xk;
        }
    }
    std::cerr << "Solution obtained in MCGDF could possibly be wrong as the loop is left by the counter exceeding maxIter=" << maxIter << std::endl;
//    exit(SIGSEGV);
    return xk;
}