#include "TermProject.h"

using std::string, std::cin, std::cout, std::endl;
using std::vector;


vector<Point> readPointsArray(const string &filename) {
    vector<string> lines;
    string line;
    vector<Point> region {};

    std::ifstream input_file(filename);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filename << "'" << std::endl;
        raise(-1);
    }

    uint32_t cnt = 0;

    while (getline(input_file, line)){
        std::istringstream ss(line);
        uint32_t pId = 0;
        double x = 0;
        double y = 0;
        ss >> pId >> x >> y;
        region.emplace_back(Point(x, y, pId));
        lines.emplace_back(line);
        ++cnt;
    }
    input_file.close();
    return region;
}

vector<BoundaryCondition> readBoundaryConditions(const string &filename) {
    vector<string> lines;
    string line;
    vector<BoundaryCondition> res {};

    std::ifstream input_file(filename);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filename << "'" << std::endl;
        raise(-1);
    }
    while (getline(input_file, line)){
        std::istringstream ss(line);
        uint32_t firstPointId;
        uint32_t secondPointId;
        uint32_t funcId;
        ss >> firstPointId >> secondPointId >> funcId;
        res.emplace_back(BoundaryCondition(firstPointId, secondPointId, funcId));
        lines.emplace_back(line);
    }
    input_file.close();
    return res;
}

vector<BoundaryCondition3> readBoundaryConditions3(std::string filename) {
    vector<string> lines;
    string line;
    vector<BoundaryCondition3> res {};

    std::ifstream input_file(filename);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filename << "'" << std::endl;
        raise(-1);
    }
    while (getline(input_file, line)){
        std::istringstream ss(line);
        uint32_t firstPointId;
        uint32_t secondPointId;
        uint32_t betaFuncId;
        uint32_t ubFuncId;
        ss >> firstPointId >> secondPointId >> betaFuncId >> ubFuncId;
        res.emplace_back(BoundaryCondition3(firstPointId, secondPointId, betaFuncId, ubFuncId));
        lines.emplace_back(line);
    }
    input_file.close();
    return res;
}

vector<FuncId> readFuncIdsArray(const string& funcArrFileName) {
    vector<string> lines;
    string line;
    vector<FuncId> res {};

    std::ifstream input_file(funcArrFileName);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << funcArrFileName << "'" << std::endl;
        raise(-1);
    }
    while (getline(input_file, line)){
        std::istringstream ss(line);
        uint32_t fFuncId;
        uint32_t gammaFuncId;
        uint32_t lambdaFuncId;
        uint32_t triangleId;
        ss >> triangleId >> fFuncId >> gammaFuncId >> lambdaFuncId;
        res.emplace_back(FuncId(fFuncId, gammaFuncId, lambdaFuncId, triangleId));
        lines.emplace_back(line);
    }
    input_file.close();
    return res;
}

uint32_t readParameter(const string &filename) {
    vector<string> lines;
    string line;
    uint32_t res = 0;

    std::ifstream input_file(filename);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filename << "'" << std::endl;
        raise(-1);
    }
    while (getline(input_file, line)){
        std::istringstream ss(line);
        uint32_t funcId;
        ss >> funcId;
        res = funcId;
        lines.emplace_back(line);
    }
    input_file.close();
    return res;
}

vector<Triangle> readTriangles(const string& filename) {
    vector<string> lines;
    string line;
    vector<Triangle> triangles {};

    std::ifstream input_file(filename);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filename << "'" << std::endl;
        raise(-1);
    }
    while (getline(input_file, line)){
        std::istringstream ss(line);
        uint32_t triangleId = 0;
        uint32_t p1i = 0;
        uint32_t p2i = 0;
        uint32_t p3i = 0;
        ss >> triangleId >> p1i >> p2i >> p3i;
        lines.emplace_back(line);
        triangles.emplace_back(Triangle(p1i, p2i, p3i, triangleId));
    }
    input_file.close();
    return triangles;
}

void writeXYFile(const string &filename, const vector<Point>& points) {
    std::ofstream outputFile(filename);
    if (!outputFile.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filename << "'" << std::endl;
        raise(-1);
    }
    for (const auto& p: points) {
        outputFile << p.id << ' ' << p.x << ' ' << p.y << endl;
    }
    outputFile.close();
}

void writeTrianglesFile(std::string filename, const vector<Triangle>& triangles) {
    std::ofstream outputFile(filename);
    if (!outputFile.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filename << "'" << std::endl;
        raise(-1);
    }
    uint32_t cnt = 0;
    for (const auto& t: triangles) {
        outputFile << cnt << ' ' << t.p1i << ' ' << t.p2i << ' ' << t.p3i << endl;
        ++cnt;
    }
    outputFile.close();
}

void writeBCondFiles(const string &bCond1Filename, const string &bCond2Filename, const string &bCond3Filename,
                     const TriangulatedProblem &triangulatedProblem) {
    std::ofstream outputFile1(bCond1Filename);
    if (!outputFile1.is_open()) {
        std::cerr << "Could not open the file - '"
                  << bCond1Filename << "'" << std::endl;
        raise(-1);
    }
    for (auto& bc: triangulatedProblem.bCond1) {
        outputFile1 << bc.firstVertexId << " " << bc.secondVertexId << " " << bc.functionId << endl;
    }
    outputFile1.close();
    std::ofstream outputFile2(bCond2Filename);
    if (!outputFile2.is_open()) {
        std::cerr << "Could not open the file - '"
                  << bCond2Filename << "'" << std::endl;
        raise(-1);
    }
    for (auto& bc: triangulatedProblem.bCond2) {
        outputFile2 << bc.firstVertexId << " " << bc.secondVertexId << " " << bc.functionId << endl;
    }
    outputFile2.close();
    std::ofstream outputFile3(bCond3Filename);
    if (!outputFile3.is_open()) {
        std::cerr << "Could not open the file - '"
                  << bCond3Filename << "'" << std::endl;
        raise(-1);
    }
    for (auto& bc: triangulatedProblem.bCond3) {
        outputFile3 << bc.firstVertexId << " " << bc.secondVertexId << " " << bc.functionBetaId << " " << bc.functionUBId << endl;
    }
    outputFile3.close();
}

void writeFuncFile(const string &funcIdsFilename, const TriangulatedProblem &triangulatedProblem) {
    std::ofstream outputFile1(funcIdsFilename);
    if (!outputFile1.is_open()) {
        std::cerr << "Could not open the file - '"
                  << funcIdsFilename << "'" << std::endl;
        raise(-1);
    }
    for (const auto& fId: triangulatedProblem.funcIds) {
        outputFile1 << fId.triangleId << " " << fId.fFuncId << " " << fId.gammaFuncId << " " << fId.lambdaFuncId << endl;
    }
    outputFile1.close();
}

TriangulatedProblem::TriangulatedProblem(const Region &reg, uint32_t nodesX, uint32_t nodesY) {
    nodes = fillWithNodes(reg.regionFigure, nodesX, nodesY);
    triangles = generateGridDelaunay(nodes);
    funcIds = vector<FuncId>{};
    uint32_t cnt = 0;
    for (const auto& t: triangles) {
        funcIds.emplace_back(FuncId(reg.fFuncId, reg.gammaFuncId, reg.lambdaFuncId, cnt));
        cnt++;
    }
    bCond1 = vector<BoundaryCondition>{};
    for (const auto& bc: reg.bCond1) {
        for (const auto &t: triangles) {
            Point p1 = nodes[bc.firstVertexId];
            Point p2 = nodes[bc.secondVertexId];
            if (checkIfEdgeBelongsToEdge(nodes[t.p1i], nodes[t.p2i], p1, p2)) {
                bCond1.emplace_back(BoundaryCondition(t.p1i, t.p2i, bc.functionId));
            }
            if (checkIfEdgeBelongsToEdge(nodes[t.p2i], nodes[t.p3i], p1, p2)) {
                bCond1.emplace_back(BoundaryCondition(t.p2i, t.p3i, bc.functionId));
            }
            if (checkIfEdgeBelongsToEdge(nodes[t.p3i], nodes[t.p1i], p1, p2)) {
                bCond1.emplace_back(BoundaryCondition(t.p3i, t.p1i, bc.functionId));
            }
        }
    }
    bCond2 = vector<BoundaryCondition>{};
    for (const auto& bc: reg.bCond2) {
        for (const auto &t: triangles) {
            Point p1 = nodes[bc.firstVertexId];
            Point p2 = nodes[bc.secondVertexId];
            if (checkIfEdgeBelongsToEdge(nodes[t.p1i], nodes[t.p2i], p1, p2)) {
                bCond2.emplace_back(BoundaryCondition(t.p1i, t.p2i, bc.functionId));
            }
            if (checkIfEdgeBelongsToEdge(nodes[t.p2i], nodes[t.p3i], p1, p2)) {
                bCond2.emplace_back(BoundaryCondition(t.p2i, t.p3i, bc.functionId));
            }
            if (checkIfEdgeBelongsToEdge(nodes[t.p3i], nodes[t.p1i], p1, p2)) {
                bCond2.emplace_back(BoundaryCondition(t.p3i, t.p1i, bc.functionId));
            }
        }
    }
    bCond3 = vector<BoundaryCondition3>{};
    for (const auto& bc: reg.bCond3) {
        for (const auto &t: triangles) {
            Point p1 = nodes[bc.firstVertexId];
            Point p2 = nodes[bc.secondVertexId];
            if (checkIfEdgeBelongsToEdge(nodes[t.p1i], nodes[t.p2i], p1, p2)) {
                bCond3.emplace_back(BoundaryCondition3(t.p1i, t.p2i, bc.functionBetaId, bc.functionUBId));
            }
            if (checkIfEdgeBelongsToEdge(nodes[t.p2i], nodes[t.p3i], p1, p2)) {
                bCond3.emplace_back(BoundaryCondition3(t.p2i, t.p3i, bc.functionBetaId, bc.functionUBId));
            }
            if (checkIfEdgeBelongsToEdge(nodes[t.p3i], nodes[t.p1i], p1, p2)) {
                bCond3.emplace_back(BoundaryCondition3(t.p3i, t.p1i, bc.functionBetaId, bc.functionUBId));
            }
        }
    }
}

void generateTriangulationFiles() {
    std::vector<Point> regionFigure = readPointsArray("../reg_figure_test.txt");
    std::vector<BoundaryCondition> bcond1 = readBoundaryConditions("../reg_bcond1.txt");
    std::vector<BoundaryCondition> bcond2 = readBoundaryConditions("../reg_bcond2.txt");
    std::vector<BoundaryCondition3> bcond3 = readBoundaryConditions3("../reg_bcond3.txt");
    uint32_t f = readParameter("../reg_f.txt");
    uint32_t gamma = readParameter("../reg_gamma.txt");
    uint32_t lambda = readParameter("../reg_lambda.txt");
    Region reg = Region(regionFigure, bcond1, bcond2, bcond3, f, gamma, lambda);
//    vector<Point> points = fillWithNodes(regionFigure, 2, 2);
    TriangulatedProblem triangulatedProblem = TriangulatedProblem(reg, 35, 35);
    writeXYFile("../xy.txt", triangulatedProblem.nodes);
    writeTrianglesFile("../triangles.txt", triangulatedProblem.triangles);
    writeBCondFiles("../bcond1.txt", "../bcond2.txt", "../bcond3.txt", triangulatedProblem);
    writeFuncFile("../f.txt", triangulatedProblem);
}

TriangulatedProblem::TriangulatedProblem(const string &prefix, const string &xyFilename,
                                         const string &trianglesFilename, const string &funcFilename,
                                         const string &bcond1Filename, const string &bcond2Filename,
                                         const string &bcond3Filename) {
    nodes = readPointsArray(prefix + xyFilename);
    triangles = readTriangles(prefix + trianglesFilename);
    bCond1 = readBoundaryConditions(prefix + bcond1Filename);
    bCond2 = readBoundaryConditions(prefix + bcond2Filename);
    bCond3 = readBoundaryConditions3(prefix + bcond3Filename);
    funcIds = readFuncIdsArray(prefix + funcFilename);
}

