#include "TermProject.h"

using std::string, std::cin, std::cout, std::endl;
using std::vector;


double getDistance(Point p1, Point p2) {
    return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}

bool Triangle::circumCircleContains(const Point& p, vector<Point> &pointList) {
    double x1 = pointList[p1i].x, x2 = pointList[p2i].x, x3 = pointList[p3i].x;
    double y1 = pointList[p1i].y, y2 = pointList[p2i].y, y3 = pointList[p3i].y;
    double xc = 0, yc = 0;
    if (x3 * (y1 - y2) + x2 * (y3 - y1) + x1 * (y2 - y3) == 0) {
        return true;
    }
    xc = ((y1 - y2) * x3 * x3 + (x1 * x1 + (y1 - y2) * (y1 - y3)) * (y2 - y3) + x2 * x2 * (y3 - y1)) /
            (2 * (x3 * (y1 - y2) + x2 * (y3 - y1) + x1 * (y2 - y3)));
    yc = (-(x2 - x3) * (x2 * x2 + y2 * y2 - x1 * x1 - y1 * y1) - (x1 - x2) * (x2 * x2 - x3 * x3 + y2 * y2 - y3 * y3))
            / (2 * (x3 * (y2 - y1) + x2 * (y1 - y3) + x1 * (y3 - y2)));
    Point p1(pointList[p1i]);
    Point pc(xc, yc);
    return getDistance(p1, pc) >= getDistance(p, pc);
}

bool Triangle::containsVertex(Point& p, vector<Point>& PointList) {
    return almostEqual(p, PointList[p1i]) || almostEqual(p, PointList[p2i]) || almostEqual(p, PointList[p3i]);
}

vector<double> fitPointsInRectangle(const vector<Point>& pointList) {
    Point p0 = pointList[0];
    double lx(p0.x), rx(p0.x), ty(p0.y), dy(p0.y);
    for (const auto& p: pointList) {
        if (p.x < lx) {
            lx = p.x;
        }
        if (p.x > rx) {
            rx = p.x;
        }
        if (p.y > ty) {
            ty = p.y;
        }
        if (p.y < dy) {
            dy = p.y;
        }
    }
    vector<double> boundaries = {lx, rx, ty, dy};
    return boundaries;
}

double gaussSquare(const vector<Point>& figure) {
    size_t len = figure.size();
    double res = 0;
    for (size_t i = 0; i < len - 1; ++i) {
        res += figure[i].x * figure[i + 1].y;
        res -= figure[i + 1].x * figure[i].y;
    }
    res += figure[len - 1].x * figure[0].y;
    res -= figure[len - 1].y * figure[0].x;
    return 0.5 * std::abs(res);
}

bool checkPointInFigure(const vector<Point>& figure, Point p) {
    double sum = 0;
    for (size_t i = 0; i < figure.size() - 1; ++i) {
        vector<Point> triang = {figure[i], figure[i + 1], p};
        sum += gaussSquare(triang);
    }
    vector<Point> triang = {figure[figure.size() - 1], figure[0], p};
    sum += gaussSquare(triang);
    return std::abs(sum - gaussSquare(figure)) < 1e-4;
}

bool comparePoints(const Point &a, const Point &b) {
    if (a.y == b.y) {
        return a.x < b.x;
    } else return a.y < b.y;
}

double getRegionLength(const vector<Point>& figure) {
    if (!figure.size()) {
        return 0;
    }
    double len = 0;
    for (auto it = figure.begin(); it < figure.end() - 1; ++it) {
        len += getDistance(*it, *(it+1));
    }
    len += getDistance(*(figure.end() - 1), *figure.begin());
    return len;
}

bool checkIfPointIsBetweenPoints(const Point &i, const Point &s, const Point &f) {
    return abs(getDistance(s, i) + getDistance(f, i) - getDistance(s, f)) < 1e-6;
}

bool checkIfEdgeBelongsToEdge(const Point &i1, const Point &i2, const Point &s, const Point &f) {
    return checkIfPointIsBetweenPoints(i1, s, f) && checkIfPointIsBetweenPoints(i2, s, f);
}

vector<Point> fillWithNodes(const vector<Point>& figure, uint32_t nodesX, uint32_t nodesY) {
    vector<Point> gridNodes(figure);
    vector<double> boundaries = fitPointsInRectangle(figure);
    double lx(boundaries[0]), rx(boundaries[1]), ty(boundaries[2]), dy(boundaries[3]);
    uint32_t cnt = figure.size();
//    double regLength = getRegionLength(figure);
////    Fill boundary with points
//    double boundStep = regLength / (2 * nodesX + 2 * nodesY);
//    auto figPointIt = figure.begin();
//    auto lastPoint = *figure.begin();
//    vector<Point> newPoints;
//    for (uint32_t iB = 0; iB < (2 * nodesX + 2 * nodesY); ++iB) {
//        double tangentX = ((figPointIt+1)->x - (figPointIt)->x) / getDistance(*figPointIt, *(figPointIt + 1));
//        double tangentY = ((figPointIt+1)->y - (figPointIt)->y) / getDistance(*figPointIt, *(figPointIt + 1));
//        Point p = Point(cnt,lastPoint.x + boundStep * tangentX, lastPoint.y + boundStep * tangentY);
//        if(checkIfPointIsBetweenPoints(lastPoint, *(figPointIt + 1), p)) {
//            lastPoint = p;
//            gridNodes.emplace_back(p);
//            ++cnt;
//        }
//        else {
//            lastPoint = *(figPointIt + 1);
//            ++figPointIt;
//        }
//    }
//    Generate points inside the region
    for (uint32_t iX = 0; iX < nodesX + 1; ++iX) {
        for(uint32_t iY = 0; iY < nodesY + 1; ++iY) {
            Point p = Point(lx + (double(iX) / nodesX) * (rx - lx), dy + (double(iY) / nodesY) * (ty - dy));
            if (checkPointInFigure(figure, p)) {
                bool new_point = true;
                for (auto & fp: figure) {
                    if (std::abs(fp.x - p.x) < 1e-5 && std::abs(fp.y - p.y) < 1e-5) {
                        new_point = false;
                    }
                }
                if (new_point) {
                    gridNodes.emplace_back(Point(p.x, p.y, cnt));
                    ++cnt;
                }
            }
        }
    }
//    gridNodes.insert(gridNodes.end(), newPoints.begin(), newPoints.end());
//    std::sort(gridNodes.begin(), gridNodes.end(), comparePoints);
//    reenumerateGrid(gridNodes);
    return gridNodes;
}

void reenumerateGrid(vector<Point>& grid) {
    for (auto cnt = 0; cnt < grid.size(); ++cnt) {
        grid[cnt].id = cnt;
    }
}

void addTriangleToEdges(vector<Point> points, vector<std::pair<uint32_t, uint32_t>>& edges) {
    edges.emplace_back(std::pair(points[0].id, points[1].id));
    edges.emplace_back(std::pair(points[1].id, points[2].id));
    edges.emplace_back(std::pair(points[2].id, points[0].id));
}

bool almostEqual(Point p1, Point p2) {
    double err = 1e-2;
    return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y) < err;
}

bool almostEqual(Edge e1, Edge e2, const vector<Point>& pointList) {
    Point p11 = pointList[e1.p1i], p12 = pointList[e1.p2i], p21 = pointList[e2.p1i], p22 = pointList[e2.p2i];
    return (almostEqual(p11, p21) && almostEqual(p12, p22)) || (almostEqual(p11, p22) && almostEqual(p12, p21));
}

bool checkIfPointBelongsToEdge(const Point& p, const Point& edgeP1, const Point& edgeP2) {
    double p1x = edgeP1.x, p1y = edgeP1.y, p2x = edgeP1.x, p2y = edgeP2.y, px = p.x, py = p.y;
    return abs((px - p1x) / (p2x - p1x) - (py - p1y) / (p2y - p1y)) < 1e-4;
}

vector<Triangle> generateGridDelaunay(const vector<Point>& pointList) {
    double minX = pointList[0].x;
    double minY = pointList[0].y;
    double maxX = minX;
    double maxY = minY;
    vector<Point> nodes = vector<Point>(pointList);
    for (std::size_t i = 0; i < pointList.size(); ++i) {
        if (pointList[i].x < minX) minX = pointList[i].x;
        if (pointList[i].y < minY) minY = pointList[i].y;
        if (pointList[i].x > maxX) maxX = pointList[i].x;
        if (pointList[i].y > maxY) maxY = pointList[i].y;
    }
    const double dx = maxX - minX;
    const double dy = maxY - minY;
    const double deltaMax = std::max(dx, dy);
    const double midx = (minX + maxX) / 2;
    const double midy = (minY + maxY) / 2;
    vector<Point> pointListTmp = pointList;

    Point p1(midx - 20 * deltaMax, midy - deltaMax, pointListTmp.size());
    Point p2(midx, midy + 20 * deltaMax, pointListTmp.size() + 1);
    Point p3(midx + 20 * deltaMax, midy - deltaMax, pointListTmp.size() + 2);

    vector<Edge> edges;
    vector<Triangle> triangles;
    pointListTmp.emplace_back(p1);
    pointListTmp.emplace_back(p2);
    pointListTmp.emplace_back(p3);
    triangles.emplace_back(Triangle(p1.id, p2.id, p3.id));

    for(auto p = begin(pointList); p != end(pointList); p++) {
        std::vector<Edge> polygon;

        for (auto & t: triangles) {
            if (t.circumCircleContains(*p, pointListTmp)) {
                t.isBad = true;
                polygon.emplace_back(Edge(pointListTmp[t.p1i].id, pointListTmp[t.p2i].id));
                polygon.emplace_back(Edge(pointListTmp[t.p2i].id, pointListTmp[t.p3i].id));
                polygon.emplace_back(Edge(pointListTmp[t.p3i].id, pointListTmp[t.p1i].id));
            }
        }

        triangles.erase(std::remove_if(begin(triangles), end(triangles), [](Triangle &t){
            return t.isBad;
        }), end(triangles));

        for(auto e1 = begin(polygon); e1 != end(polygon); ++e1) {
            for(auto e2 = e1 + 1; e2 != end(polygon); ++e2) {
                if (almostEqual(*e1, *e2, pointListTmp)) {
                    e1->isBad = true;
                    e2->isBad = true;
                }
            }
        }
        polygon.erase(std::remove_if(begin(polygon), end(polygon), [](Edge &e){
            return e.isBad;
        }), end(polygon));

        for(const auto e : polygon)
            triangles.emplace_back(Triangle(e.p1i, e.p2i, p->id));
    }

    for (auto it = triangles.end() - 1; it != triangles.begin() - 1; it--) {
        if (it->containsVertex(p1, pointListTmp) || it->containsVertex(p2, pointListTmp) ||
            it->containsVertex(p3, pointListTmp))
            it = triangles.erase(it);
    }

    return triangles;
}

