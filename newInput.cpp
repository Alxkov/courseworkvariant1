#include "TermProject.h"

uint32_t addPointToVectorIfPossible(vector<Point>& v, const Point& p) {
    for (uint32_t cnt = 0; cnt < v.size(); ++cnt) {
        if (almostEqual(v[cnt], p)) {
            return cnt;
        }
    }
    v.emplace_back(p);
    return v.size() - 1;
}

bool checkIfPointIsInVector(vector<Point>& v, const Point& p) {
    for (auto& el: v) {
        if (almostEqual(p, el)) {
            return true;
        }
    }
    return false;
}

uint32_t getNodesRowLength(const Subregion& subregion, vector<Point>& nodes) {
    uint32_t res = 0;
    double initY = nodes[subregion.nodeIds[0]].y;
    while (nodes[subregion.nodeIds[res]].y == initY) {
        ++res;
    }
    return res;
}

void insertNewNodes(vector<double>& v, uint32_t numberOfNodes) {
    for(uint32_t cnt = 0; cnt < numberOfNodes; ++cnt) {
        vector<double> distances;
        for (auto it = v.begin(); it < v.end() - 1; ++it) {
            distances.emplace_back((*(it + 1) - *it));
        }
        auto maxIt = std::max_element(distances.begin(), distances.end());
        v.emplace_back((*(v.begin() + (maxIt - distances.begin())) + *(v.begin() + (maxIt - distances.begin()) + 1)) / 2);
        sort(v.begin(), v.end());
    }
}

void triangulateSubregion(const Subregion &subregion, vector<Point> &nodes, vector<Triangle> &triangles,
                          uint32_t subregionId) {
    uint32_t len = getNodesRowLength(subregion, nodes);
    uint32_t nTriangles = triangles.size();
    uint32_t trCnt = 0;
    for (uint32_t cntVert = 0; cntVert < (subregion.nodeIds.size() - 1) / len; ++cntVert) {
        for (uint32_t cnt = 0; cnt < len - 1; ++cnt) {
            triangles.emplace_back(
                    Triangle(subregion.nodeIds[cnt + cntVert * len], subregion.nodeIds[cnt + 1 + cntVert * len], subregion.nodeIds[cnt + len + cntVert * len], trCnt + nTriangles, subregionId));
            triangles.emplace_back(Triangle(subregion.nodeIds[cnt + 1 + cntVert * len], subregion.nodeIds[cnt + len + cntVert * len],
                                            subregion.nodeIds[cnt + len + 1 + cntVert * len], trCnt + 1 + nTriangles, subregionId));
            trCnt += 2;
        }
    }
}

RegionNew readRegionNewFile(const string &filenameRegion) {
    vector<string> lines;
    string line;

    std::ifstream input_file(filenameRegion);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filenameRegion << "'" << std::endl;
        raise(-1);
    }

    RegionNew regionNew;

    uint32_t xWArrayLength = 0;
    uint32_t yWArrayLength = 0;
    uint32_t nOfRegions = 0;

    getline(input_file, line);
    std::istringstream ssx(line);
    ssx >> xWArrayLength;
    getline(input_file, line);
    std::istringstream ssx1(line);
    for (uint32_t cnt = 0; cnt < xWArrayLength; ++cnt) {
        double xWValue = 0;
        ssx1 >> xWValue;
        regionNew.xWarray.emplace_back(xWValue);
    }

    getline(input_file, line);
    std::istringstream ssy(line);
    ssy >> yWArrayLength;
    getline(input_file, line);
    std::istringstream ssy1(line);
    for (uint32_t cnt = 0; cnt < yWArrayLength; ++cnt) {
        double yWValue = 0;
        ssy1 >> yWValue;
        regionNew.yWarray.emplace_back(yWValue);
    }

    getline(input_file, line);
    std::istringstream ssn(line);
    ssn >> nOfRegions;

    for (uint32_t cnt = 0; cnt < nOfRegions; ++cnt) {
        uint32_t regionId = 0;
        uint32_t xWl = 0, xWr = 0, yWd = 0, yWt = 0;
        getline(input_file, line);
        std::istringstream ssr(line);
        ssr >> regionId >> xWl >> xWr >> yWd >> yWt;
        regionNew.subregions.emplace_back(Subregion({Point(regionNew.xWarray[xWl], regionNew.yWarray[yWd]), Point(regionNew.xWarray[xWr], regionNew.yWarray[yWd]), Point(regionNew.xWarray[xWr], regionNew.yWarray[yWt]), Point(regionNew.xWarray[xWl], regionNew.yWarray[yWt])}, cnt));
    }

    input_file.close();

    return regionNew;
}

void readBCondNewFiles(const string& filenameBcond, RegionNew& regionNew,
                       vector<BoundaryCondition>& bcond1ArrTmp, vector<BoundaryCondition> &bcond2ArrTmp, vector<BoundaryCondition3> &bcond3ArrTmp,
                       vector<double>& xWarrayOld, vector<double>& yWarrayOld) {
    vector<string> lines;
    string line;

    std::ifstream input_file_bcond(filenameBcond);
    if (! input_file_bcond.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filenameBcond << "'" << std::endl;
        raise(-1);
    }

    while(getline(input_file_bcond, line)) {
        std::istringstream ssbcond(line);
        uint32_t bcondtype = 0;
        ssbcond >> bcondtype;
        if (bcondtype == 1) {
            uint32_t funcId = 0, xfirst = 0, xlast = 0, yfirst = 0, ylast = 0;
            ssbcond >> funcId >> xfirst >> xlast >> yfirst >> ylast;
            uint32_t firstId = addPointToVectorIfPossible(regionNew.nodes, Point(xWarrayOld[xfirst], yWarrayOld[yfirst]));
            uint32_t lastId = addPointToVectorIfPossible(regionNew.nodes, Point(xWarrayOld[xlast], yWarrayOld[ylast]));
            auto bcond = BoundaryCondition(firstId, lastId, funcId);
            bcond1ArrTmp.emplace_back(bcond);
        }
        if (bcondtype == 2) {
            uint32_t funcId = 0, xfirst = 0, xlast = 0, yfirst = 0, ylast = 0;
            ssbcond >> funcId >> xfirst >> xlast >> yfirst >> ylast;
            uint32_t firstId = addPointToVectorIfPossible(regionNew.nodes, Point(xWarrayOld[xfirst], yWarrayOld[yfirst]));
            uint32_t lastId = addPointToVectorIfPossible(regionNew.nodes, Point(xWarrayOld[xlast], yWarrayOld[ylast]));
            auto bcond = BoundaryCondition(firstId, lastId, funcId);
            bcond2ArrTmp.emplace_back(bcond);
        }
        if (bcondtype == 3) {
            uint32_t functionBetaId = 0, functionUBId = 0, xfirst = 0, xlast = 0, yfirst = 0, ylast = 0;
            ssbcond >> functionBetaId >> functionUBId >> xfirst >> xlast >> yfirst >> ylast;
            uint32_t firstId = addPointToVectorIfPossible(regionNew.nodes, Point(xWarrayOld[xfirst], yWarrayOld[yfirst]));
            uint32_t lastId = addPointToVectorIfPossible(regionNew.nodes, Point(xWarrayOld[xlast], yWarrayOld[ylast]));
            auto bcond = BoundaryCondition3(firstId, lastId, functionBetaId, functionUBId);
            bcond3ArrTmp.emplace_back(bcond);
        }
    }
    input_file_bcond.close();

}

void readFGammaLambdaNewFiles(const string& filename, TriangulatedProblem& triangulatedProblem) {
    vector<string> lines;
    string line;

    std::ifstream input_file_funcIds(filename);
    if (!input_file_funcIds.is_open()) {
        std::cerr << "Could not open the file - '"
                  << filename << "'" << std::endl;
        raise(-1);
    }

    while(getline(input_file_funcIds, line)) {
        std::istringstream ssFuncIds(line);
        uint32_t regionId = 0, funcFId = 0, funcGammaId = 0, funcLambdaId = 0;
        ssFuncIds >> regionId >> funcFId >> funcGammaId >> funcLambdaId;
        for (auto& t: triangulatedProblem.triangles) {
            if (t.subregionId == regionId) {
                triangulatedProblem.funcIds.emplace_back(FuncId(funcFId, funcGammaId, funcLambdaId, t.id));
            }
        }
    }
}

void scaleBoundaryConditionToTriangulatedProblem (TriangulatedProblem& triangulatedProblem, RegionNew& regionNew,
        vector<BoundaryCondition>& bcond1ArrTmp, vector<BoundaryCondition>& bcond2ArrTmp, vector<BoundaryCondition3>& bcond3ArrTmp) {
    for (const auto& bc: bcond1ArrTmp) {
        for (const auto &t: triangulatedProblem.triangles) {
            Point p1 = regionNew.nodes[bc.firstVertexId];
            Point p2 = regionNew.nodes[bc.secondVertexId];
            if (checkIfEdgeBelongsToEdge(regionNew.nodes[t.p1i], regionNew.nodes[t.p2i], p1, p2)) {
                triangulatedProblem.bCond1.emplace_back(BoundaryCondition(t.p1i, t.p2i, bc.functionId));
            }
            if (checkIfEdgeBelongsToEdge(regionNew.nodes[t.p2i], regionNew.nodes[t.p3i], p1, p2)) {
                triangulatedProblem.bCond1.emplace_back(BoundaryCondition(t.p2i, t.p3i, bc.functionId));
            }
            if (checkIfEdgeBelongsToEdge(regionNew.nodes[t.p3i], regionNew.nodes[t.p1i], p1, p2)) {
                triangulatedProblem.bCond1.emplace_back(BoundaryCondition(t.p3i, t.p1i, bc.functionId));
            }
        }
    }

    for (const auto& bc: bcond2ArrTmp) {
        for (const auto &t: triangulatedProblem.triangles) {
            Point p1 = regionNew.nodes[bc.firstVertexId];
            Point p2 = regionNew.nodes[bc.secondVertexId];
            if (checkIfEdgeBelongsToEdge(regionNew.nodes[t.p1i], regionNew.nodes[t.p2i], p1, p2)) {
                triangulatedProblem.bCond2.emplace_back(BoundaryCondition(t.p1i, t.p2i, bc.functionId));
            }
            if (checkIfEdgeBelongsToEdge(regionNew.nodes[t.p2i], regionNew.nodes[t.p3i], p1, p2)) {
                triangulatedProblem.bCond2.emplace_back(BoundaryCondition(t.p2i, t.p3i, bc.functionId));
            }
            if (checkIfEdgeBelongsToEdge(regionNew.nodes[t.p3i], regionNew.nodes[t.p1i], p1, p2)) {
                triangulatedProblem.bCond2.emplace_back(BoundaryCondition(t.p3i, t.p1i, bc.functionId));
            }
        }
    }

    for (const auto& bc: bcond3ArrTmp) {
        for (const auto &t: triangulatedProblem.triangles) {
            Point p1 = regionNew.nodes[bc.firstVertexId];
            Point p2 = regionNew.nodes[bc.secondVertexId];
            if (checkIfEdgeBelongsToEdge(regionNew.nodes[t.p1i], regionNew.nodes[t.p2i], p1, p2)) {
                triangulatedProblem.bCond3.emplace_back(
                        BoundaryCondition3(t.p1i, t.p2i, bc.functionBetaId, bc.functionUBId));
            }
            if (checkIfEdgeBelongsToEdge(regionNew.nodes[t.p2i], regionNew.nodes[t.p3i], p1, p2)) {
                triangulatedProblem.bCond3.emplace_back(
                        BoundaryCondition3(t.p2i, t.p3i, bc.functionBetaId, bc.functionUBId));
            }
            if (checkIfEdgeBelongsToEdge(regionNew.nodes[t.p3i], regionNew.nodes[t.p1i], p1, p2)) {
                triangulatedProblem.bCond3.emplace_back(
                        BoundaryCondition3(t.p3i, t.p1i, bc.functionBetaId, bc.functionUBId));
            }
        }
    }
}

void addNodesToSubregion(RegionNew& regionNew) {
    uint32_t nodesCnt = 0;
    for (auto& yW: regionNew.yWarray) {
        for (auto& xW: regionNew.xWarray) {
            auto p = Point(xW, yW, nodesCnt);
            for (auto& subreg: regionNew.subregions) {
                if (checkPointInFigure(subreg.figure, p)) {
                    if (!checkIfPointIsInVector(regionNew.nodes, p)) {
                        ++nodesCnt;
                    }
                    subreg.nodeIds.emplace_back(addPointToVectorIfPossible(regionNew.nodes, p));
                }
            }
        }
    }
}


TriangulatedProblem readInputFilesNew(const string& folder, uint32_t newNodesX, uint32_t newNodesY) {
    RegionNew regionNew = readRegionNewFile(folder + "\\region.txt");

    vector<double> xWarrayOld = regionNew.xWarray;
    vector<double> yWarrayOld = regionNew.yWarray;

    regionNew.xWarray = generateNonUniformalInterval(1, 3, 15, 0.9);
    regionNew.yWarray = generateNonUniformalInterval(1, 3, 15, 0.9);
//    insertNewNodes(regionNew.xWarray, newNodesX);
//    insertNewNodes(regionNew.yWarray, newNodesY);


    addNodesToSubregion(regionNew);

    TriangulatedProblem triangulatedProblem(regionNew.nodes);

    vector<BoundaryCondition> bcond1ArrTmp;
    vector<BoundaryCondition> bcond2ArrTmp;
    vector<BoundaryCondition3> bcond3ArrTmp;

    readBCondNewFiles(folder + "\\bconds.txt", regionNew, bcond1ArrTmp, bcond2ArrTmp, bcond3ArrTmp, xWarrayOld, yWarrayOld);

    for (uint32_t cnt = 0; cnt < regionNew.subregions.size(); ++cnt) {
        triangulateSubregion(regionNew.subregions[cnt], regionNew.nodes, triangulatedProblem.triangles, cnt);
    }
    scaleBoundaryConditionToTriangulatedProblem(triangulatedProblem, regionNew, bcond1ArrTmp, bcond2ArrTmp, bcond3ArrTmp);
    readFGammaLambdaNewFiles(folder + "\\fgammalambda.txt", triangulatedProblem);
    return triangulatedProblem;
}

void generateTriangulationFilesNew(const string& folder, uint32_t newNodesX, uint32_t newNodesY) {
    TriangulatedProblem triangulatedProblem = readInputFilesNew(folder, newNodesX, newNodesY);
    writeXYFile(folder + "xy.txt", triangulatedProblem.nodes);
    writeTrianglesFile(folder + "triangles.txt", triangulatedProblem.triangles);
    writeBCondFiles(folder + "bcond1.txt", folder + "bcond2.txt", folder + "bcond3.txt", triangulatedProblem);
    writeFuncFile(folder + "f.txt", triangulatedProblem);
}

bool checkIfPointBelongsToRegionNew(const RegionNew& regionNew, const Point& point) {
    for (auto& subreg: regionNew.subregions) {
        if (checkPointInFigure(subreg.figure, point)) {
            return true;
        }
    }
    return false;
}