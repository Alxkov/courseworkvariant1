#include "TermProject.h"

RightVectorOnTriangle::RightVectorOnTriangle(const TriangulatedProblem &triangulatedProblem, uint32_t triangleId) {
    this->triangleId = triangleId;
    Point p1 = triangulatedProblem.nodes[triangulatedProblem.triangles[triangleId].p1i];
    Point p2 = triangulatedProblem.nodes[triangulatedProblem.triangles[triangleId].p2i];
    Point p3 = triangulatedProblem.nodes[triangulatedProblem.triangles[triangleId].p3i];
    vector<vector<double>> alphaArray = getAlphaArray(p1, p2, p3);
    double detD = getDetD(p1, p2, p3);
    bVector = {0, 0, 0};
    for (size_t i = 0; i < 3; ++i) {
        bVector[i] = (funcVec[triangulatedProblem.funcIds[triangleId].fFuncId](p1.x, p1.y) +
                      funcVec[triangulatedProblem.funcIds[triangleId].fFuncId](p2.x, p2.y) +
                      funcVec[triangulatedProblem.funcIds[triangleId].fFuncId](p3.x, p3.y)) /3 /6  * abs(detD);
    }
}
