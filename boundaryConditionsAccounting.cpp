#include "TermProject.h"

void boundaryConditionAccount2(const TriangulatedProblem& triangulatedProblem, SparseMatrix& matrix, vector<double>& rightVector) {
    for (auto& bc2: triangulatedProblem.bCond2) {
        Point p1 = triangulatedProblem.nodes[bc2.firstVertexId];
        Point p2 = triangulatedProblem.nodes[bc2.secondVertexId];
        double l = getDistance(p1, p2);
        double value = (funcVec[bc2.functionId](p1.x, p1.y) + funcVec[bc2.functionId](p2.x, p2.y)) / 2 * l / 2;
        rightVector[bc2.firstVertexId] += value;
        rightVector[bc2.secondVertexId] += value;
    }
}

void boundaryConditionAccount3(const TriangulatedProblem& triangulatedProblem, SparseMatrix& matrix, vector<double>& rightVector) {
    for (auto& bc3: triangulatedProblem.bCond3) {
        Point p1 = triangulatedProblem.nodes[bc3.firstVertexId];
        Point p2 = triangulatedProblem.nodes[bc3.secondVertexId];
        double l = getDistance(p1, p2);
        double valueAii = (funcVec[bc3.functionBetaId](p1.x, p1.y) + funcVec[bc3.functionBetaId](p2.x, p2.y)) / 2 * l / 3;
        double valueAij = (funcVec[bc3.functionBetaId](p1.x, p1.y) + funcVec[bc3.functionBetaId](p2.x, p2.y)) / 2 * l / 6;
        matrix.addToEl(bc3.firstVertexId, bc3.firstVertexId, valueAii);
        matrix.addToEl(bc3.secondVertexId, bc3.secondVertexId, valueAii);
        matrix.addToEl(bc3.firstVertexId, bc3.secondVertexId, valueAij);
        matrix.addToEl(bc3.secondVertexId, bc3.firstVertexId, valueAij);
        double valuebi = (funcVec[bc3.functionBetaId](p1.x, p1.y) + funcVec[bc3.functionBetaId](p2.x, p2.y)) / 2 *
                (funcVec[bc3.functionUBId](p1.x, p1.y) + funcVec[bc3.functionUBId](p2.x, p2.y)) / 2 * l / 2;
        rightVector[bc3.firstVertexId] += valuebi;
        rightVector[bc3.secondVertexId] += valuebi;
    }
}

void boundaryConditionAccount1(const TriangulatedProblem& triangulatedProblem, SparseMatrix& matrix, vector<double>& rightVector) {
    for (auto& bc1: triangulatedProblem.bCond1) {
        Point p1 = triangulatedProblem.nodes[bc1.firstVertexId];
        Point p2 = triangulatedProblem.nodes[bc1.secondVertexId];
        matrix.setRow0(bc1.firstVertexId);
        matrix.setRow0(bc1.secondVertexId);
        matrix.setEl(bc1.firstVertexId, bc1.firstVertexId, 1);
        matrix.setEl(bc1.secondVertexId, bc1.secondVertexId, 1);
        rightVector[bc1.firstVertexId] = funcVec[bc1.functionId](p1.x, p1.y);
        rightVector[bc1.secondVertexId] = funcVec[bc1.functionId](p2.x, p2.y);
    }
}

void boundaryConditionAccount(const TriangulatedProblem& triangulatedProblem, SparseMatrix& matrix, vector<double>& rightVector) {
    boundaryConditionAccount2(triangulatedProblem, matrix, rightVector);
    boundaryConditionAccount3(triangulatedProblem, matrix, rightVector);
    boundaryConditionAccount1(triangulatedProblem, matrix, rightVector);
}