#include "TermProject.h"

/* ARGSUSED1 */
GLboolean doubleBuffer;

void Key(unsigned char key, int x, int y)
{
    switch (key) {
        case 27:
            exit(0);
        case 'g':
        case 'G':
            drawGridSwitcher = !drawGridSwitcher;
            break;
        case 'a':
        case 'A':
            drawAxesSwitcher = !drawAxesSwitcher;
            break;
        case '+':
            scale = scale * 1.11111111111111111111111111112;
            break;
        case '-':
            scale = scale * 0.9;
            break;
        case 'b':
        case 'B':
            showBoundariesSwitcher = !showBoundariesSwitcher;
            break;
        case 'n':
        case 'N':
            drawNodesSwitcher = !drawNodesSwitcher;
            break;
        default:
            return;
    }

}

void SpecialInput(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_UP:
            frameCenter = Point(frameCenter.x, frameCenter.y + 0.1);
            break;
        case GLUT_KEY_DOWN:
            frameCenter = Point(frameCenter.x, frameCenter.y - 0.1);
            break;
        case GLUT_KEY_LEFT:
            frameCenter = Point(frameCenter.x - 0.1, frameCenter.y);
            break;
        case GLUT_KEY_RIGHT:
            frameCenter = Point(frameCenter.x + 0.1, frameCenter.y);
            break;
        default:
            break;
    }
}

void MouseMovingCallBack(int x, int y) {
    mouseCoordsDisplay = Point(-1 + 2 * double(x) / glutGet(GLUT_WINDOW_WIDTH), 1 - 2 * double(y) / glutGet(GLUT_WINDOW_HEIGHT), 0);
    mouseCoordsReal = displayPointToReal(mouseCoordsDisplay);
}

void resize(int width, int height) {
    // we ignore the params and do:
    glutReshapeWindow( width, height);
    TwWindowSize(width, height);
    if (height == 0)
        height = 1;

    double ratio =  width * 1.0 / height;

    glViewport(0, 0, width, height);
    // Set the viewport to be the entire window
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-ratio, ratio, -1.0, 1.0, -0.0, 0.0);
    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    Draw();
}

void mouseClickCallBack(int button, int state, int x, int y) {
    if (button == 3) {
        scale *= 1.111111111112;
    }
    if (button == 4) {
        scale *= 0.9;
    }
}

void RenderString(double x, double y, void *font, const char *string)
{
    char *c;

    glColor3f(0, 0, 0);
    glRasterPos2f(x, y);

    glutBitmapString(font, reinterpret_cast<const unsigned char *>(string));
}


void Draw()
{
    glutSpecialFunc(SpecialInput);
    rescaleAll(scale, frameCenter);

//    RedisplayTweakBar();

    auto v = glutGet(GLUT_VERSION);
    glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    
    glStencilFunc(GL_ALWAYS, 1, 1);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    glPointSize(3);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    paintRegionWithSolutionValueOpt(10);
    //    Triangulation
    if (drawGridSwitcher) {
        for(auto& t: triangleListFrame) {
            glBegin (GL_LINE_LOOP);
            glColor3ub(50, 50, 50);
            glVertex2f(pointListToDrawFrame[t.p1i].x, pointListToDrawFrame[t.p1i].y);
            glVertex2f(pointListToDrawFrame[t.p2i].x, pointListToDrawFrame[t.p2i].y);
            glVertex2f(pointListToDrawFrame[t.p3i].x, pointListToDrawFrame[t.p3i].y);
            glEnd();
        }
    }

    //    Region boundaries
    if (showBoundariesSwitcher) {
        if (!regionPointsListFrame.empty()) {
            glColor3ub(0,  0, 0);
            glBegin(GL_LINE_LOOP);
            for (auto& p: regionPointsListFrame) {
                glVertex2f(p.x, p.y);
            }
            glEnd();
        }
        if(!globalRegionNew.subregions.empty()) {
            glColor3ub(0,  0, 0);
            for (auto& subreg: globalRegionNew.subregions) {
                glBegin(GL_LINE_LOOP);
                for (auto& p: subreg.figure) {
                    Point displayPoint = realPointToDisplay(p);
                    glVertex2f(displayPoint.x, displayPoint.y);
                }
                glEnd();
            }
        }
    }

    //    Nodes
    if (drawNodesSwitcher) {
        glColor3ub(200, 0, 0);
        glBegin (GL_POINTS);
        for(auto& p: pointListToDrawFrame) {
            glVertex2f(p.x, p.y);
        }
        glEnd();
    }

//        Axes
    if (drawAxesSwitcher) {
        glColor3ub(0, 0, 0);
        glBegin (GL_LINE_STRIP);
        glVertex2f(axesFrame.xStart.x, axesFrame.xStart.y);
        glVertex2f(axesFrame.xFinish.x, axesFrame.xFinish.y);
        glEnd();
        glBegin (GL_LINE_STRIP);
        glVertex2f(axesFrame.yStart.x, axesFrame.yStart.y);
        glVertex2f(axesFrame.yFinish.x, axesFrame.yFinish.y);
        glEnd();
    }

    char output[50];

    drawScaleBar(-0.95, 0.85);
    drawColorPalette(-0.95, 0.5);
    drawMouseRegionInspect(-0.95, 0.8);

    mouseFrameMove();
//    TwDraw();

    if (doubleBuffer) {
        glutSwapBuffers();
    } else {
        glFlush();
    }
    glutPostRedisplay();
}

extern GLboolean doubleBuffer;

void Args(int argc, char **argv)
{
    GLint i;
    doubleBuffer = GL_TRUE;
    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-sb") == 0) {
            doubleBuffer = GL_FALSE;
        } else if (strcmp(argv[i], "-db") == 0) {
            doubleBuffer = GL_TRUE;
        }
    }
}

int drawAll(int argc, char **argv)
{
    GLenum type;

    glutInit(&argc, argv);
    Args(argc, argv);
    drawAxesSwitcher = false;

//    InitTweakBar();

    type = GLUT_RGB;
    type |= (doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE;
    glutInitDisplayMode(type);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Variant 1");
//    glutReshapeWindow(600, 600);

    glClearColor(1.0, 1.0, 1.0, 0.0);
    glClearStencil(0);
    glStencilMask(1);
//    glEnable(GL_STENCIL_TEST);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1.0, 1.0, -1.0, 1.0, -0.0, 0.0);
    glMatrixMode(GL_MODELVIEW);

    glutKeyboardFunc(Key);
    glutPassiveMotionFunc(MouseMovingCallBack);
    glutDisplayFunc(Draw);
    glutReshapeFunc(resize);
    glutMouseFunc(mouseClickCallBack);
    glutMainLoop();
    return 0;             /* ANSI C requires main to return int. */
}