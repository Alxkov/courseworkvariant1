#include "TermProject.h"

double SolvedProblem::operator()(double x, double y) {
    // point is within a triangle
    for (auto& tr: triangulatedProblem.triangles) {
        vector<Point> figure = {triangulatedProblem.nodes[tr.p1i], triangulatedProblem.nodes[tr.p2i], triangulatedProblem.nodes[tr.p3i]};
        if (checkPointInFigure(figure, Point(x, y))) {
            Point p1 = triangulatedProblem.nodes[triangulatedProblem.triangles[tr.id].p1i];
            Point p2 = triangulatedProblem.nodes[triangulatedProblem.triangles[tr.id].p2i];
            Point p3 = triangulatedProblem.nodes[triangulatedProblem.triangles[tr.id].p3i];
            vector<vector<double>> alphaArray = getAlphaArray(p1, p2, p3);
            return (problemSolution[tr.p1i] * (alphaArray[0][0] + x * alphaArray[0][1] + y * alphaArray[0][2]) +
                    problemSolution[tr.p2i] * (alphaArray[1][0] + x * alphaArray[1][1] + y * alphaArray[1][2]) +
                    problemSolution[tr.p3i] * (alphaArray[2][0] + x * alphaArray[2][1] + y * alphaArray[2][2]));
        }
    }
    return 0;
}

double vectorNorm(const vector<double>& v, uint32_t skip) {
    double res = 0;
    uint32_t skipCnt = 0;
    for (auto& el: v) {
        if(skipCnt >= skip - 1) {
            skipCnt = 0;
            res += el * el;
        }
        else {
            ++skipCnt;
        }
    }
    return sqrt(res);
}

vector<double> SolvedProblem::computeInAllNodes() {
    vector<double> res = {};
    for (auto& n: triangulatedProblem.nodes) {
        res.emplace_back((*this)(n.x, n.y));
    }
    return res;
}

vector<double> SolvedProblem::computeInAllNodes(const vector<Point>& nodes) {
    vector<double> res = {};
    for (auto& n: nodes) {
        res.emplace_back((*this)(n.x, n.y));
    }
    return res;
}

vector<double> SolvedProblem::computeGivenFunctionInAllNodes(double (*f)(double, double)) {
    vector<double> res = {};
    for (auto& n: triangulatedProblem.nodes) {
        res.emplace_back((*f)(n.x, n.y));
    }
    return res;
}

vector<double> SolvedProblem::computeGivenFunctionInAllNodes(double (*f)(double, double), const vector<Point>& nodes) {
    vector<double> res = {};
    for (auto& n: nodes) {
        res.emplace_back((*f)(n.x, n.y));
    }
    return res;
}

double SolvedProblem::computerErrorByPreciseSolution(double (*f)(double, double), uint32_t skip) {
    vector<double> qh = (*this).computeInAllNodes();
    vector<double> qprec = (*this).computeGivenFunctionInAllNodes(f);
    vector<double> diff = {};
    for (uint32_t cnt = 0; cnt < qh.size(); ++cnt) {
        diff.emplace_back(qh[cnt] - qprec[cnt]);
    }
    return vectorNorm(diff, skip)/vectorNorm(qprec, skip);
}

double SolvedProblem::computerErrorByPreciseSolution(double (*f)(double, double), uint32_t skip, const vector<Point>& nodes) {
    vector<double> qh = (*this).computeInAllNodes(nodes);
    vector<double> qprec = (*this).computeGivenFunctionInAllNodes(f, nodes);
    vector<double> diff = {};
    for (uint32_t cnt = 0; cnt < qh.size(); ++cnt) {
        diff.emplace_back(qh[cnt] - qprec[cnt]);
    }
    return vectorNorm(diff, skip)/vectorNorm(qprec, skip);
}

void
SolvedProblem::getLatexTable(double minX, double maxX, double minY, double maxY, uint32_t xNodes, uint32_t yNodes) {
    cout << "\\begin{table}[H]" << endl;
    cout << "\\begin{tabular}{|l|";
    for (uint32_t cnt2 = 0; cnt2 < xNodes + 1; ++cnt2) {
        cout << "|l";
    }
    cout << "|}" << endl;
    cout << "\\hline" << endl;
    cout << "\\backslashbox{y}{x}" << endl;
    cout << "&";
    for (uint32_t cnt2 = 0; cnt2 < xNodes - 1; ++cnt2) {
        cout << minX + double(cnt2) / (xNodes - 1) * (maxX - minX)  << " & ";
    }
    cout << maxX << "\\\\" << endl << "\\hline\\hline" << endl;
    for (uint32_t cnt1 = 0; cnt1 < yNodes; ++cnt1) {
        cout << minY + double(cnt1) / (yNodes - 1) * (maxY - minY) << " & ";
        for (uint32_t cnt2 = 0; cnt2 < xNodes - 1; ++cnt2) {
            double x = minX + double(cnt2) / (xNodes - 1) * (maxX - minX);
            double y = minY + double(cnt1) / (yNodes - 1) * (maxY - minY);
            cout << (*this)(x, y) << " & ";
        }
        double x = maxX;
        double y =  minY + double(cnt1) / (yNodes - 1) * (maxY - minY);
        cout << (*this)(x, y);
        cout << " \\\\" << endl;
        cout << "\\hline " << endl;
    }
    cout << "\\end{tabular}" << endl;
    cout << "\\end{table}";
}

vector<Point> generateNodesInIntervalUniform(double minX, double maxX, uint32_t nx, double minY, double maxY,  uint32_t ny) {
    vector<Point> res;
    for (uint32_t cnt1 = 0; cnt1 < ny; ++cnt1) {
        for (uint32_t cnt2 = 0; cnt2 < nx; ++cnt2) {
            double x = minX + (maxX - minX) * cnt2 / (nx - 1);
            double y = minY + (maxY - minY) * cnt1 / (ny - 1);
            res.emplace_back(Point(x, y));
        }
    }
    return res;
}

vector<double> generateNonUniformalInterval(double min, double max, uint32_t n, double q) {
    double lengthTotal = (1 - pow((q), n)) / (1 - q) / (max - min);
    double lengthSmallest = pow(q, n) / lengthTotal;
    vector<double> res;
    for (auto cnt = 1; cnt <= n; ++cnt) {
        res.emplace_back(min + lengthSmallest * (1 - pow(1/q, cnt)) / (1 - 1 / q));
    }
    res.emplace_back(max);
    return res;
}