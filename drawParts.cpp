#include "TermProject.h"

void mouseFrameMove() {
    double windowSpeed = 1e-3;
    double moveBoundary = 0.85;
    if (mouseCoordsDisplay.x < -moveBoundary){
        frameCenter.x -= windowSpeed / scale;
    }

    if (mouseCoordsDisplay.x > moveBoundary){
        frameCenter.x += windowSpeed / scale;
    }

    if (mouseCoordsDisplay.y < -moveBoundary){
        frameCenter.y -= windowSpeed / scale;
    }

    if (mouseCoordsDisplay.y > moveBoundary){
        frameCenter.y += windowSpeed / scale;
    }
}

void drawScaleBar(double x, double y) {
    char output[50];
    snprintf(output, 50, "scale");
    RenderString(x, y, GLUT_BITMAP_HELVETICA_12, output);
    glColor3ub(255, 0, 0);
    glBegin (GL_LINE_STRIP);
    glLineWidth(10);
    double barLength = 0.2;
    glVertex2f(x + 0.15, y);
    glVertex2f(x + 0.15 + barLength, y);
    glLineWidth(2);
    glEnd();
    snprintf(output, 50, "%2.2f", barLength / scale);
    RenderString(x + 0.15 + barLength / 3.5, y + 0.01, GLUT_BITMAP_HELVETICA_12, output);

}

void drawMouseRegionInspect(double x, double y) {
    char output[50];
    snprintf(output, 50, "mouse coordinates: \nx: %2.2f, y: %2.2f", mouseCoordsReal.x, mouseCoordsReal.y);
    RenderString(x, y, GLUT_BITMAP_HELVETICA_12, output);

    if (checkIfPointBelongsToRegionNew(globalRegionNew, mouseCoordsReal)) {
        snprintf(output, 50, "solution(%2.2f, %2.2f): is: %2.2f", mouseCoordsReal.x, mouseCoordsReal.y, (*solvedProblem)(mouseCoordsReal.x, mouseCoordsReal.y));
        RenderString(-0.95, y - 0.1, GLUT_BITMAP_HELVETICA_12, output);
    }
    else {
        snprintf(output, 50, "Mouse pointer is outside of figure region");
        RenderString(-0.95, y - 0.1, GLUT_BITMAP_HELVETICA_12, output);
    }
}

void drawColorPalette(double x, double y) {
    uint32_t numberOfBars = 15;
    double barWidth = 0.08, barHeight = 0.08;
    for (uint32_t cnt = 0; cnt < numberOfBars; ++cnt) {
        double barValue = minSolutionValue + cnt * (maxSolutionValue * 1.01 - minSolutionValue) / (numberOfBars - 1);
        ColorRGB color = hslToRGB(functionFrom0To360(barValue, minSolutionValue, maxSolutionValue * 1.01), 1, 0.5);
        glColor4ub(color.red, color.green, color.blue, 255);
        glBegin(GL_QUADS);
        glVertex2f(x, y - cnt * barHeight);
        glVertex2f(x + barHeight, y - cnt * barHeight);
        glVertex2f(x + barHeight, y - (cnt + 1) * barHeight);
        glVertex2f(x, y - (cnt + 1) * barHeight);
        glEnd();
        char output[50];
        snprintf(output, 50, "%2.2f", barValue);
        RenderString(x + barWidth + 0.02, y - (cnt + 0.6) * barHeight, GLUT_BITMAP_HELVETICA_12, output);
    }
    for (uint32_t cnt = 0; cnt < numberOfBars; ++cnt) {
        glColor3ub(0, 0, 0);
        glBegin(GL_LINES);
        glVertex2f(x, y - (cnt + 1) * barHeight);
        glVertex2f(x + barHeight, y - (cnt + 1) * barHeight);
        glEnd();
    }

    glColor3ub(0, 0, 0);
    glBegin(GL_LINE_LOOP);
    glVertex2f(x, y);
    glVertex2f(x + barHeight, y);
    glVertex2f(x + barHeight, y - numberOfBars * barHeight);
    glVertex2f(x, y - numberOfBars * barHeight);
    glEnd();

}

void TW_CALL MoveUpwards(void * /*clientData*/)
{
    double windowSpeed = 1e-3;
    double moveBoundary = 0.85;
    frameCenter.x += windowSpeed / scale;
    cout << "moved" << endl;
}

void InitTweakBar() {
    TwInit(TW_OPENGL, NULL);
    bar = TwNewBar("Controls");

    // set a bar values width
    int width = glutGet(GLUT_WINDOW_WIDTH) * 1 /6; // pixels
    TwSetParam(bar, NULL, "valueswidth", TW_PARAM_INT32, 1, &width);

    int barPos[2] = {glutGet(GLUT_WINDOW_WIDTH) * 13 / 20, glutGet(GLUT_WINDOW_HEIGHT) * 1 / 20};
    TwSetParam(bar, NULL, "position", TW_PARAM_INT32, 2, &barPos);
    TwAddVarRW(bar, "Zoom", TW_TYPE_DOUBLE, &scale,"");
    TwAddVarRW(bar, "Mouse X:", TW_TYPE_DOUBLE, &(mouseCoordsReal.x),"");
    TwAddVarRW(bar, "Mouse Y:", TW_TYPE_DOUBLE, &(mouseCoordsReal.y),"");
    TwAddButton(bar, "Run", MoveUpwards, nullptr, " label='->' ");
}

void RedisplayTweakBar() {
    int width = glutGet(GLUT_WINDOW_WIDTH) * 1 /6; // pixels
    TwSetParam(bar, NULL, "valueswidth", TW_PARAM_INT32, 1, &width);

    int barPos[2] = {glutGet(GLUT_WINDOW_WIDTH) * 13 /20, glutGet(GLUT_WINDOW_HEIGHT) * 1 / 20};
    TwSetParam(bar, NULL, "position", TW_PARAM_INT32, 2, &barPos);

}