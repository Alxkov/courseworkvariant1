#include "TermProject.h"

using std::string, std::cin, std::cout, std::endl;
using std::vector;

double function0(double x, double y) {
    return x * x + y * y;
}

double function1(double x, double y) {
    return x + y;
}

double function2(double x, double y) {
    return 1;
}

double function3(double x, double y) {
    return -6 * x - 6 * y + x * x + y * y;
}

double function4(double x, double y) {
    return sin(x + y);
}

double function5(double x, double y) {
    return -2 * cos(x + y) + 2 * (x + y) * sin(x + y) + sin(x + y);
}

double function6(double x, double y) {
    return 3;
}

double function7(double x, double y) {
    return -1;
}

double function8(double x, double y) {
    return 12;
}

double function9(double x, double y) {
    return 3 * y;
}

double function10(double x, double y) {
    return -6 * x;
}
