#include <iostream>
#include "TermProject.h"


int main(int argc, char** argv) {
    funcVec = {function0, function1, function2, function3, function4, function5, function6, function7, function8};
    generateTriangulationFiles();
//    auto region = readPointsArray("../reg_figure1.txt");
    TriangulatedProblem triangulatedProblem = TriangulatedProblem("../", "xy.txt",
                                                                  "triangles.txt",
                                                                  "f.txt",
                                                                  "bcond1.txt",
                                                                  "bcond2.txt",
                                                                  "bcond3.txt");
    auto gM = generateGlobalMatrix(triangulatedProblem);
    auto rV = generateRightVector(triangulatedProblem);
//    gM.printMatrixPlusVector(rV);
    boundaryConditionAccount(triangulatedProblem,gM, rV);
//    gM.printMatrixPlusVector(rV);
//    vector<double> solution = {0, 4, -7.49837, -12.9978};
    vector<double> solution = solveSystemMCGDF(gM, rV, 1e-4, 1e5);
    for (auto& c: solution) {
        cout << c << " ";
    }
    cout << endl;
    auto solvedProblem = SolvedProblem(triangulatedProblem, solution);
//    cout << std::fixed << solvedProblem(0, 0) << endl;
//    cout << std::fixed << solvedProblem(4, 0) << endl;
//    cout << std::fixed << solvedProblem(1, 1) << endl;
//    cout << std::fixed << solvedProblem(3.9, 3.9) << endl;
//    cout << std::fixed << solvedProblem(3.75, 3.75) << endl;
//    cout << std::fixed << solvedProblem(0.01, 0.01) << endl;
//    auto sM = SparseMatrix(6);
//    sM.setEl(0, 0,1);
//    sM.setEl(0, 1,2);
//    sM.setEl(0, 3, 3);
//    sM.setEl(1, 3, 6.078);
//    sM.setEl(1, 0, 4);
//    sM.setEl(1, 1, 5);
//    sM.setEl(1, 5, 7);
//    sM.setEl(2, 2, 8);
//    sM.setEl(2, 5, 9);
//    sM.setEl(3, 0, 10);
//    sM.setEl(3, 1, 11);
//    sM.setEl(3, 3, 12);
//    sM.setEl(3, 4, 13);
//    sM.printMatrix();
//    cout << funcVec[0](1, 2);
//    auto a = getAlphaArray(Point(0, 0), Point(0,1), Point(1,0));
    //    regionPointsList = vector<Point>(region);
//    triangleList = vector<Triangle>(triangulatedProblem.triangles);
//    pointListToDraw = vector<Point>(triangulatedProblem.nodes);
//    drawGrid(argc, argv);

//    auto nodes = fillWithNodes(inpRegionFigure, 3, 3);
//    for (auto& n: nodes) {
//        cout <<"i: "<< n.id << " x: "<< n.x << " y: " << n.y << endl;
//    }
//    cout << "triangs" << endl;
//    auto triangulation = generateGridDelaunay(nodes);
//        for (auto& t: triangulatedProblem.triangles) {
//        cout << " x: "<< triangulatedProblem.nodes[t.p1i].x << " y: " << triangulatedProblem.nodes[t.p1i].y << " Point 2: ";
//        cout << " x: "<< triangulatedProblem.nodes[t.p2i].x << " y: " << triangulatedProblem.nodes[t.p2i].y << " Point 3: ";
//        cout << " x: "<< triangulatedProblem.nodes[t.p3i].x << " y: " << triangulatedProblem.nodes[t.p3i].y << endl;
//    }
    return 0;
}
