//
// Created by Alexander on 5/15/2021.
//
#include <string>
#include <sstream>
#include <string>
#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <limits>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <GL/freeglut.h>
#define PI 3.14151828
#include <iomanip>
#include <AntTweakBar.h>
#include <cstdarg>

#ifndef TERMPROJECT_TERMPROJECT_H
#define TERMPROJECT_TERMPROJECT_H

#define WIDTH 600
#define HEIGHT 600

using std::vector, std::cout, std::cin ,std::endl, std::string;

void Key(unsigned char key, int x, int y);
void Draw();
void Args(int argc, char **argv);
int drawAll(int argc, char **argv);

class Point {
public:
    u_int32_t id;
    double x;
    double y;
    void pointSet(double x, double y) {
        x = x;
        y = y;
    }
    void pointSet(u_int32_t number, double x, double y) {
        number = number;
        x = x;
        y = y;
    }
//    Point(double x, double y): x(x), y(y){};
    Point(double x, double y, u_int32_t id=0) : id(id), x(x), y(y) {};
};

class Edge {
public:
    uint32_t p1i;
    uint32_t p2i;
    bool isBad;
    Edge(uint32_t p1i, uint32_t p2i): p1i(p1i), p2i(p2i) { isBad = false;};
};

bool almostEqual(Edge e1, Edge e2, const vector<Point>& pointList);
bool almostEqual(Point p1, Point p2);

class Triangle {
public:
    uint32_t p1i;
    uint32_t p2i;
    uint32_t p3i;
    uint32_t id;
    uint32_t subregionId;
    bool isBad;
    bool circumCircleContains(const Point& p, vector<Point> &pointList);
    bool containsVertex(Point& p, vector<Point>& PointList);
    Triangle(uint32_t p1i, uint32_t p2i, uint32_t p3i): p1i(p1i), p2i(p2i), p3i(p3i) {
        isBad = false;
        subregionId = 0;
        id = 0;
    };
    Triangle(uint32_t p1i, uint32_t p2i, uint32_t p3i, uint32_t id): p1i(p1i), p2i(p2i), p3i(p3i), id(id) {
        isBad = false;
        subregionId = 0;
    };
    Triangle(uint32_t p1i, uint32_t p2i, uint32_t p3i, uint32_t id, uint32_t subregionId): p1i(p1i), p2i(p2i), p3i(p3i), id(id), subregionId(subregionId) {
        isBad = false;
    };
};

class BoundaryCondition {
public:
    uint32_t firstVertexId;
    uint32_t secondVertexId;
    uint32_t functionId;
    BoundaryCondition(uint32_t firstVertexId, uint32_t secondVertexId, uint32_t functionId): firstVertexId(firstVertexId), secondVertexId(secondVertexId), functionId(functionId) {};
};

class BoundaryCondition3 {
public:
    uint32_t firstVertexId;
    uint32_t secondVertexId;
    uint32_t functionBetaId;
    uint32_t functionUBId;
    BoundaryCondition3(uint32_t firstVertexId, uint32_t secondVertexId, uint32_t functionBetaId, uint32_t functionUBId):
    firstVertexId(firstVertexId), secondVertexId(secondVertexId), functionBetaId(functionBetaId), functionUBId(functionUBId) {};
};

class Region {
public:
    vector<Point> regionFigure;
    vector<BoundaryCondition> bCond1;
    vector<BoundaryCondition> bCond2;
    vector<BoundaryCondition3> bCond3;
    uint32_t fFuncId;
    uint32_t gammaFuncId;
    uint32_t lambdaFuncId;
    Region(vector<Point>& regionFigure, vector<BoundaryCondition>& bCond1, vector<BoundaryCondition>& bCond2,
           vector<BoundaryCondition3>& bCond3, uint32_t fFuncId, uint32_t gammaFuncId, uint32_t lambdaFuncId):
    regionFigure(regionFigure),
    bCond1(bCond1),
    bCond2(bCond2),
    bCond3(bCond3),
    fFuncId(fFuncId),
    gammaFuncId(gammaFuncId),
    lambdaFuncId(lambdaFuncId){
    };
};

class Subregion {
public:
    vector<Point> figure;
    uint32_t id;
    vector<double> nodeIds;
    Subregion(vector<Point> figure, uint32_t id): figure(std::move(figure)), id(id) {};
};

class RegionNew {
public:
    vector<Subregion> subregions;
    vector<double> xWarray;
    vector<double> yWarray;
    vector<Point> nodes;
};

class FuncId {
public:
    uint32_t fFuncId;
    uint32_t gammaFuncId;
    uint32_t lambdaFuncId;
    uint32_t triangleId;
    FuncId(uint32_t fFuncId, uint32_t gammaFuncId, uint32_t lambdaFuncId, uint32_t triangleId):
    fFuncId(fFuncId),
    gammaFuncId(gammaFuncId),
    lambdaFuncId(lambdaFuncId),
    triangleId(triangleId)
    {
    }
};

class TriangulatedProblem {
public:
    vector<Point> nodes;
    vector<Triangle> triangles;
    vector<BoundaryCondition> bCond1;
    vector<BoundaryCondition> bCond2;
    vector<BoundaryCondition3> bCond3;
    vector<FuncId> funcIds;
    TriangulatedProblem(const Region &reg, uint32_t nodesX, uint32_t nodesY);
    TriangulatedProblem(const string &prefix, const string &xyFilename,
                        const string &trianglesFilename, const string &funcFilename,
                        const string &bcond1Filename, const string &bcond2Filename,
                        const string &bcond3Filename);
    explicit TriangulatedProblem(vector<Point> nodes): nodes(std::move(nodes)){};
};

std::vector<Point> readPointsArray(const string &filename);
vector<BoundaryCondition> readBoundaryConditions(const string &filename);
uint32_t readParameter(const string &filename);
vector<FuncId> readFuncIdsArray(const string& funcArrFileName);
vector<BoundaryCondition3> readBoundaryConditions3(std::string filename);
vector<Triangle> readTriangles(const string& filename);
void writeTrianglesFile(std::string filename, const vector<Triangle> &triangles);
void writeXYFile(const string &filename, const vector<Point>& points);
void writeBCond1File(std::string filename, const vector<Point>& points, const vector<Triangle>& triangles, const vector<BoundaryCondition>& bConds);
void writeBCondFiles(const string &bCond1Filename, const string &bCond2Filename, const string &bCond3Filename,
                     const TriangulatedProblem &triangulatedProblem);
void writeFuncFile(const string &funcIdsFilename, const TriangulatedProblem &triangulatedProblem);

vector<double> fitPointsInRectangle(const vector<Point>& pointList);
double gaussSquare(const vector<Point>& figure);
bool checkPointInFigure(const vector<Point>& figure, Point p);
vector<Point> fillWithNodes(const vector<Point>& figure, uint32_t nodesX = 6, uint32_t nodesY = 6);
double getDistance(Point p1, Point p2);
double getRegionLength(const vector<Point>& figure);
bool checkIfPointIsBetweenPoints(const Point &i, const Point &s, const Point &f);
bool checkIfPointBelongsToEdge(const Point& p, const Point& edgeP1, const Point& edgeP2);
bool checkIfEdgeBelongsToEdge(const Point &i1, const Point &i2, const Point &s, const Point &f);

bool comparePoints(const Point &a, const Point &b);
void reenumerateGrid(vector<Point>& grid);
void addTriangleToEdges(vector<Point> points, vector<std::pair<uint32_t, uint32_t>>& edges);

vector<Triangle> generateGridDelaunay(const vector<Point>& pointList);

double function0(double x, double y);
double function1(double x, double y);
double function2(double x, double y);
double function3(double x, double y);
double function4(double x, double y);
double function5(double x, double y);
double function6(double x, double y);
double function7(double x, double y);
double function8(double x, double y);
double function9(double x, double y);
double function10(double x, double y);

typedef double (*fp)(double, double);
inline vector<fp> funcVec;

class LocalMatricesOnTriangle {
public:
    vector<vector<double>> matrixG;
    vector<vector<double>> matrixM;
    uint32_t triangleId;
    LocalMatricesOnTriangle(const TriangulatedProblem &triangulatedProblem, uint32_t triangleId);
};

class RightVectorOnTriangle {
public:
    vector<double> bVector;
    uint32_t triangleId;
    RightVectorOnTriangle(const TriangulatedProblem &triangulatedProblem, uint32_t triangleId);
};

double getDetD(const Point& p1, const Point& p2, const Point& p3);
vector<vector<double>> getAlphaArray(const Point& p1, const Point& p2, const Point& p3);

class SparseMatrix {
    uint32_t size;
    vector<double> di;
    vector<double> al, au;
    vector<uint32_t> ja, ia;
public:
//    double operator()(uint32_t rowN, uint32_t colN);
    double getEl(uint32_t rowN, uint32_t colN);
    void setEl(uint32_t rowN, uint32_t colN, double el);
    void setRow0(uint32_t rowN);
    void addToEl(uint32_t rowN, uint32_t colN, double toAdd);
    void printMatrix();
    void printMatrixPlusVector(const vector<double>& rVector);
    void printMatrixPlusVectorWf(const vector<double>& rVector);
    vector<double> operator*(const vector<double>& vector);
    SparseMatrix operator*(double c);
    SparseMatrix(const SparseMatrix& matrix);
    uint32_t getSize();
    explicit SparseMatrix(uint32_t size);
};

SparseMatrix generateGlobalMatrix(const TriangulatedProblem& triangulatedProblem);
vector<double> generateRightVector(const TriangulatedProblem& triangulatedProblem);


void boundaryConditionAccount2(const TriangulatedProblem& triangulatedProblem, SparseMatrix& matrix, vector<double>& rightVector);
void boundaryConditionAccount3(const TriangulatedProblem& triangulatedProblem, SparseMatrix& matrix, vector<double>& rightVector);
void boundaryConditionAccount1(const TriangulatedProblem& triangulatedProblem, SparseMatrix& matrix, vector<double>& rightVector);
void boundaryConditionAccount(const TriangulatedProblem& triangulatedProblem, SparseMatrix& matrix, vector<double>& rightVector);

vector<double> getDiagonalConditioningInverseVector(SparseMatrix matrix, const vector<double>& q);
vector<double> addTwoVectors(const vector<double>& v1, const vector<double>& v2);
vector<double> vectorMultiplyOnDouble(const vector<double>& v1, double c);
double vectorsScalarProduct(const vector<double>& v1, const vector<double>& v2);
double vectorDistance(const vector<double>& v1, const vector<double>& v2);
vector<double> solveSystemMCGDF(SparseMatrix matrix, const vector<double>& rVector, double eps, uint32_t maxIter);

class SolvedProblem {
    TriangulatedProblem triangulatedProblem;
    vector<double> problemSolution;
public:
    double operator()(double x, double y);
    void getLatexTable(double minX, double maxX, double minY, double maxY, uint32_t xNodes, uint32_t yNodes);
    SolvedProblem(TriangulatedProblem  triangulatedProblem, vector<double> problemSolution):
            triangulatedProblem(std::move(triangulatedProblem)),
            problemSolution(std::move(problemSolution)){};

    vector<double> computeInAllNodes();

    vector<double> computeGivenFunctionInAllNodes(double (*f)(double, double));

    double computerErrorByPreciseSolution(double (*f)(double, double));

    double computerErrorByPreciseSolution(double (*f)(double, double), uint32_t skip);

    vector<double> computeInAllNodes(const vector<Point> &nodes);

    vector<double> computeGivenFunctionInAllNodes(double (*f)(double, double), const vector<Point> &nodes);

    double computerErrorByPreciseSolution(double (*f)(double, double), uint32_t skip, const vector<Point> &nodes);
};

class Axes {
public:
    Point xStart;
    Point xFinish;
    Point yStart;
    Point yFinish;
    Axes(const Point& xStart, const Point& xFinish, const Point& yStart, const Point& yFinish):
    xStart(xStart), xFinish(xFinish), yStart(yStart), yFinish(yFinish) {};
    Axes(): xStart(Point(-1, 0)), xFinish(Point(1, 0)), yStart(Point(0, -1)), yFinish(Point(0, 1)) {};
    void resizeInFrame(const Point& newXStart, const Point& newXFinish, const Point& newYStart, const Point& newYFinish);
    void resizeInFrame(double newScale, const Point& newFrameCenter);
};

class ColorRGB {
public:
    const uint8_t red;
    const uint8_t green;
    const uint8_t blue;
    ColorRGB(uint8_t red, uint8_t green, uint8_t blue): red(red), green(green), blue(blue) {};
};

ColorRGB hslToRGB(double h, double s, double l);

inline double scale;
inline Point frameCenter(0, 0, 0);
inline Axes axesFrame;

inline vector<Point> regionPointsList;

inline RegionNew globalRegionNew;
inline vector<Triangle> triangleList;
inline vector<Point> pointListToDraw;

inline vector<Point> regionPointsListFrame;
inline vector<Triangle> triangleListFrame;
inline vector<Point> pointListToDrawFrame;
inline vector<ColorRGB> valuesInDrawPoints;

inline SolvedProblem* solvedProblem;

static bool drawAxesSwitcher;
static bool drawNodesSwitcher;
static bool drawGridSwitcher;
static bool drawBoundariesSwitcher;
static bool showBoundariesSwitcher;
inline Point mouseCoordsReal(Point(0, 0, 0));
inline Point mouseCoordsDisplay(Point(0, 0, 0));
inline double maxSolutionValue;
inline double minSolutionValue;
inline TwBar *bar;

Point displayPointToReal(const Point& p);
Point realPointToDisplay(const Point& p);
void resize(int width, int height);
void mouseFrameMove();
void drawScaleBar(double x, double y);
void drawMouseRegionInspect(double x, double y);
void drawColorPalette(double x, double y);

vector<Point> scalePointsToFrame(const vector<Point> &points);
void initGlobals();
void rescaleAll(double newScale, const Point& newCenter);
void RenderString(double x, double y, void *font, const char *string);
double functionFrom0To360(double value, double min, double max);
void paintRegionWithSolutionValue(double resolution);
void fillTriangleWithSolution(const Triangle& triangle);
void paintRegionWithSolutionValueOpt(double resolution);
void InitTweakBar();
void RedisplayTweakBar();

uint32_t addPointToVectorIfPossible(vector<Point>& v, const Point& p);
bool checkIfPointIsInVector(vector<Point>& v, const Point& p);

uint32_t getNodesRowLength(const Subregion& subregion, vector<Point>& nodes);
void triangulateSubregion(const Subregion &subregion, vector<Point> &nodes, vector<Triangle> &triangles,
                          uint32_t subregionId);
void insertNewNodes(vector<double>& v, uint32_t numberOfNodes);
TriangulatedProblem readInputFilesNew(const string& folder, uint32_t newNodesX, uint32_t newNodesY);
void generateTriangulationFilesNew(const string& folder, uint32_t newNodesX, uint32_t newNodesY);
RegionNew readRegionNewFile(const string &filenameRegion);
bool checkIfPointBelongsToRegionNew(const RegionNew& regionNew, const Point& point);
vector<Point> generateNodesInIntervalUniform(double minX, double maxX, uint32_t nx, double minY, double maxY,  uint32_t ny);
vector<double> generateNonUniformalInterval(double min, double max, uint32_t n, double q);

#endif //TERMPROJECT_TERMPROJECT_H
