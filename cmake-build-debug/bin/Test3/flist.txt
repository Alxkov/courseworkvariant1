double function0(double x, double y) {
    return 2 * y + 3 * x ;
}

double function1(double x, double y) {
    return -2;
}

double function2(double x, double y) {
    return 0;
}

double function3(double x, double y) {
    return y;
}

double function4(double x, double y) {
    return 2 * y;
}

double function5(double x, double y) {
    return -3;
}

double function6(double x, double y) {
    return -3;
}

double function7(double x, double y) {
    return 0.5;
}

double function8(double x, double y) {
    return 3 * x + 4 * y - 8;
}

double function9(double x, double y) {
    return 2;
}

double function10(double x, double y) {
    return 3 * x + 4 * y + 1.5;
}