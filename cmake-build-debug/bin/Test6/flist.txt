double function0(double x, double y) {
    return 3 * y + 2 * x;
}

double function1(double x, double y) {
    return 2 * y;
}

double function2(double x, double y) {
    return y + 2 * x + 4;
}

double function3(double x, double y) {
    return 6 * y;
}

double function4(double x, double y) {
    return -6;
}

double function5(double x, double y) {
    return 0;
}

double function6(double x, double y) {
    return -6 * y;
}

double function7(double x, double y) {
    return 6 * y;
}

double function8(double x, double y) {
    return -2;
}

double function9(double x, double y) {
    return y + 0.5;
}

double function10(double x, double y) {
    return -6 * x;
}