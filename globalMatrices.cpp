#include "TermProject.h"

SparseMatrix::SparseMatrix(uint32_t size): size(size){
    di = vector<double>(size, 0.0);
    ia = vector<uint32_t>(size + 1, 0);
}

double SparseMatrix::getEl(uint32_t rowN, uint32_t colN) {
    if (rowN >= size || colN >= size) {
        std::cerr << "Matrix indices: (" << rowN << "," << colN << ")" << " exceed matrix size: " << size  <<  endl;
        raise(SIGSEGV);
    }
    if (rowN == colN) return di[rowN];
    if (rowN > colN) { // lower triangle
        uint32_t rowBeginInd = ia[rowN];
        uint32_t rowEndInd = ia[rowN + 1];
        for (uint32_t cnt = rowBeginInd; cnt < rowEndInd; ++cnt) {
            if(ja[cnt] == colN) {
                return al[cnt];
            }
        }
        return 0;
    }
    uint32_t rowBeginInd = ia[colN];
    uint32_t rowEndInd = ia[colN + 1];
    for (uint32_t cnt = rowBeginInd; cnt < rowEndInd; ++cnt) {
        if(ja[cnt] == rowN) {
            return au[cnt];
        }
    }
    return 0;
}



void SparseMatrix::setEl(uint32_t rowN, uint32_t colN, double el) {
    if (rowN >= size || colN >= size) {
        std::cerr << "Matrix indices: (" << rowN << "," << colN << ")" << " exceed matrix size: " << size  <<  endl;
        raise(SIGSEGV);
    }
    if (rowN == colN) {
        di[rowN] = el;
        return;
    }
    if (rowN > colN) { // lower triangle
        uint32_t rowBeginInd = ia[rowN];
        uint32_t rowEndInd = ia[rowN + 1];
        auto itAl = al.begin() + rowBeginInd;
        auto itAu = au.begin() + rowBeginInd;
        auto itJa = ja.begin() + rowBeginInd;
        for (uint32_t elInd = rowBeginInd; elInd < rowEndInd; ++elInd) {
            if (ja[elInd] < colN) {
                itAl++;
                itAu++;
                itJa++;
            } else if (ja[elInd] == colN) {
                al[elInd] = el;
                return;
            }
            else if (ja[elInd] > colN) break;
        }
        for (uint32_t cnt = rowN + 1; cnt <= size; ++cnt) {
            ++ia[cnt];
        }
        al.insert(itAl, el);
        au.insert(itAu, 0);
        ja.insert(itJa, colN);
    }
    if (rowN < colN) { // upper triangle
        uint32_t colBeginInd = ia[colN];
        uint32_t colEndInd = ia[colN + 1];
        auto itAl = al.begin() + colBeginInd;
        auto itAu = au.begin() + colBeginInd;
        auto itJa = ja.begin() + colBeginInd;
        for (uint32_t elInd = colBeginInd; elInd < colEndInd; ++elInd) {
            if (ja[elInd] < rowN) {
                itAl++;
                itAu++;
                itJa++;
            } else if (ja[elInd] == rowN) {
                au[elInd] = el;
                return;
            }
            else if (ja[elInd] > rowN) break;
        }
        for (uint32_t cnt = colN + 1; cnt <= size; ++cnt) {
            ++ia[cnt];
        }
        al.insert(itAl, 0);
        au.insert(itAu, el);
        ja.insert(itJa, rowN);
    }
}

void SparseMatrix::setRow0(uint32_t rowN) {
    //works correctly only in boundaryConditionAccount1, where the profile of Matrix is initially symmetric
    for (uint32_t colN = 0; colN < size; ++colN) {
        if (getEl(rowN, colN) != 0) {
            setEl(rowN, colN, 0);
        }
    }
}

void SparseMatrix::addToEl(uint32_t rowN, uint32_t colN, double toAdd){
    double currentEl = getEl(rowN, colN);
    setEl(rowN, colN, currentEl + toAdd);
}

void SparseMatrix::printMatrix() {
    for(uint32_t rowCnt = 0; rowCnt < size; ++rowCnt) {
        cout << "|";
        for(uint32_t colCnt = 0; colCnt < size; ++colCnt) {
            cout << std::setprecision(2) << std::setw(5) <<  getEl(rowCnt, colCnt) << " ";
        }
        cout << "|" << endl;
    }
}

void SparseMatrix::printMatrixPlusVector(const vector<double>& rVector){
    cout << " ";
    for(uint32_t colCnt = 0; colCnt < size; ++colCnt) {
        cout << "______";
    }
    cout << "   _____" << endl;
    for(uint32_t rowCnt = 0; rowCnt < size; ++rowCnt) {
        cout << "|";
        for(uint32_t colCnt = 0; colCnt < size; ++colCnt) {
            cout << std::setprecision(3) << std::setw(5) <<  getEl(rowCnt, colCnt) << " ";
        }
        cout << "| |" << std::setprecision(3) << std::setw(5) << rVector[rowCnt] << "|" << endl;
    }
    cout << " ";
    for(uint32_t colCnt = 0; colCnt < size; ++colCnt) {
        cout << "______";
    }
    cout << "   _____" << endl;
}

void SparseMatrix::printMatrixPlusVectorWf(const vector<double>& rVector){
    cout << " ";
    for(uint32_t colCnt = 0; colCnt < size; ++colCnt) {
        cout << "______";
    }
    cout << "   _____" << endl;
    for(uint32_t rowCnt = 0; rowCnt < size; ++rowCnt) {
        cout << "{";
        for(uint32_t colCnt = 0; colCnt < size - 1; ++colCnt) {
            cout << std::setprecision(5) << std::setw(7) <<  getEl(rowCnt, colCnt) << ", ";
        }
        cout << std::setprecision(5) << std::setw(7) <<  getEl(rowCnt, size -1);
        cout <<  "},"<< endl;
    }
    cout << " ";
    for(uint32_t colCnt = 0; colCnt < size; ++colCnt) {
        cout << rVector[colCnt] << ", ";
    }
    cout << endl;
}

vector<double> SparseMatrix::operator*(const vector<double>& v){
    vector<double> res = vector<double>(size, 0);
    for (auto it = ia.begin(); it < ia.end() - 1; ++it) {
        for (uint32_t cnt = 0; cnt < *(it + 1) - *it; ++cnt) {
            res[it - ia.begin()] += al[*(it) + cnt] * v[ja[*(it) + cnt]];
            res[ja[*(it) + cnt]] += au[*(it) + cnt] * v[it - ia.begin()];
        }
    }
    for (uint32_t cnt = 0; cnt < size; ++cnt){
        res[cnt] += di[cnt] * v[cnt];
    }
    return res;
}


SparseMatrix SparseMatrix::operator*(const double c) {
    SparseMatrix res = SparseMatrix(*this);
    for (auto& el: res.di) {
        el *= c;
    }
    for (auto& el: res.au) {
        el *= c;
    }
    for (auto& el: res.al) {
        el *= c;
    }
    return res;
}

SparseMatrix::SparseMatrix(const SparseMatrix& matrix) {
    size = matrix.size;
    di = vector<double>(matrix.di);
    au = vector<double>(matrix.au);
    al = vector<double>(matrix.al);
    ia = vector<uint32_t >(matrix.ia);
    ja = vector<uint32_t >(matrix.ja);

}

uint32_t SparseMatrix::getSize() {
    return size;
}

SparseMatrix generateGlobalMatrix(const TriangulatedProblem& triangulatedProblem) {
    SparseMatrix res = SparseMatrix(triangulatedProblem.nodes.size());
    vector<LocalMatricesOnTriangle> localMatricesArray;
    uint32_t cnt = 0;
    for (auto& tr: triangulatedProblem.triangles) {
        auto lM = LocalMatricesOnTriangle(triangulatedProblem, tr.id);
        vector<uint32_t> pointIds = {tr.p1i, tr.p2i, tr.p3i};
        for (uint32_t cnt1 = 0; cnt1 < 3; ++cnt1) {
            for (uint32_t cnt2 = 0; cnt2 < 3; ++cnt2) {
                res.addToEl(pointIds[cnt1], pointIds[cnt2], lM.matrixG[cnt1][cnt2] + lM.matrixM[cnt1][cnt2]);
            }
        }
        localMatricesArray.emplace_back(lM);
        ++cnt;
    }
    return res;
}

vector<double> generateRightVector(const TriangulatedProblem& triangulatedProblem){
    vector<double> res = vector<double>(triangulatedProblem.nodes.size(), 0);
    for (auto &tr: triangulatedProblem.triangles) {
        auto lV = RightVectorOnTriangle(triangulatedProblem, tr.id);
        vector<uint32_t> pointIds = {tr.p1i, tr.p2i, tr.p3i};
        for (uint32_t cnt1 = 0; cnt1 < 3; ++cnt1) {
            res[pointIds[cnt1]] += lV.bVector[cnt1];
        }
    }
    return res;
}