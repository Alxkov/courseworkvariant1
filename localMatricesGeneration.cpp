#include "TermProject.h"

double getDetD(const Point& p1, const Point& p2, const Point& p3) {
    return -p2.x * p1.y + p3.x * p1.y + p1.x * p2.y - p3.x * p2.y - p1.x * p3.y + p2.x * p3.y;
}

vector<vector<double>> getAlphaArray(const Point& p1, const Point& p2, const Point& p3) {
    double detD = getDetD(p1, p2, p3);
    vector<vector<double>> res = {{p2.x * p3.y - p3.x * p2.y, p2.y - p3.y, p3.x - p2.x},
                                  {p3.x * p1.y - p1.x * p3.y, p3.y - p1.y, p1.x - p3.x},
                                  {p1.x * p2.y - p2.x * p1.y, p1.y - p2.y, p2.x - p1.x}};
    for (auto& row: res) {
        for(auto& el: row) {
            el = el / detD;
        }
    }
    return res;
}

LocalMatricesOnTriangle::LocalMatricesOnTriangle(const TriangulatedProblem &triangulatedProblem, uint32_t triangleId) {
    this->triangleId = triangleId;
    Point p1 = triangulatedProblem.nodes[triangulatedProblem.triangles[triangleId].p1i];
    Point p2 = triangulatedProblem.nodes[triangulatedProblem.triangles[triangleId].p2i];
    Point p3 = triangulatedProblem.nodes[triangulatedProblem.triangles[triangleId].p3i];
    vector<vector<double>> alphaArray = getAlphaArray(p1, p2, p3);
    double detD = getDetD(p1, p2, p3);
    matrixG = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    matrixM = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    for (size_t i = 0; i < 3; ++i) {
        for(size_t j = 0; j < 3; ++j) {
            matrixG[i][j] = (alphaArray[i][1] * alphaArray[j][1] + alphaArray[i][2] * alphaArray[j][2]) *
                    (funcVec[triangulatedProblem.funcIds[triangleId].lambdaFuncId](p1.x, p1.y) +
                            funcVec[triangulatedProblem.funcIds[triangleId].lambdaFuncId](p2.x, p2.y) +
                            funcVec[triangulatedProblem.funcIds[triangleId].lambdaFuncId](p3.x, p3.y)) * abs(detD) / 6;
            matrixM[i][j] = (funcVec[triangulatedProblem.funcIds[triangleId].gammaFuncId](p1.x, p1.y) +
                             funcVec[triangulatedProblem.funcIds[triangleId].gammaFuncId](p2.x, p2.y) +
                             funcVec[triangulatedProblem.funcIds[triangleId].gammaFuncId](p3.x, p3.y)) /3  * abs(detD) / 12;
            if (i != j) {
                matrixM[i][j] /= 2;
            }
        }
    }
}