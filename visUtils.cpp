#include "TermProject.h"

void Axes::resizeInFrame(const Point& newXStart, const Point& newXFinish, const Point& newYStart, const Point& newYFinish) {
    this->xStart = Point(newXStart);
    this->xFinish = Point(newXFinish);
    this->yStart = Point(newYStart);
    this->yFinish = Point(newYFinish);
}

void Axes::resizeInFrame(double newScale, const Point& newFrameCenter) {
    double span = 1e5;
    this->xStart = Point(newScale * (-span - newFrameCenter.x), newScale * (0 - newFrameCenter.y));
    this->xFinish = Point(newScale * (span - newFrameCenter.x), newScale * (0 - newFrameCenter.y));
    this->yStart = Point(newScale * (0 - newFrameCenter.x), newScale * (-span - newFrameCenter.y));
    this->yFinish = Point(newScale * (0 - newFrameCenter.x), newScale * (span - newFrameCenter.y));
}

vector<Point> scalePointsToFrame(const vector<Point> &points) {
    vector<Point> res;
    for (auto& p: points) {
        res.emplace_back(realPointToDisplay(p));
//        res.emplace_back(Point(newScale * (p.x - frameCenter.x), newScale * (p.y - frameCenter.y), p.id));
    }
    return res;
}

void initGlobals() {
    frameCenter = Point(0, 0, 0);
    scale = 1;
    regionPointsListFrame = scalePointsToFrame(regionPointsList);
    triangleListFrame = triangleList;
    pointListToDrawFrame = scalePointsToFrame(pointListToDraw);
    showBoundariesSwitcher = false;
    drawNodesSwitcher = false;
    drawGridSwitcher = false;
    drawBoundariesSwitcher = false;

    vector<double> valuesInPoints = {};
    for (const auto& p: pointListToDraw) {
        valuesInPoints.emplace_back((*solvedProblem)(p.x, p.y));
    }
    auto minValueIt = std::min_element(valuesInPoints.begin(), valuesInPoints.end());
    auto maxValueIt = std::max_element(valuesInPoints.begin(), valuesInPoints.end());
    maxSolutionValue = (*(maxValueIt)) * (1 + 1e-2);
    minSolutionValue = *(minValueIt);
    for (const auto& p: pointListToDraw) {
        valuesInDrawPoints.emplace_back(hslToRGB(functionFrom0To360(((*solvedProblem)(p.x, p.y)), minSolutionValue, maxSolutionValue), 1, 0.5));
    }
}

void rescaleAll(double newScale, const Point& newCenter) {
    regionPointsListFrame = scalePointsToFrame(regionPointsList);
    pointListToDrawFrame = scalePointsToFrame(pointListToDraw);
    axesFrame.resizeInFrame(newScale, newCenter);
}

double functionFrom0To360(double value, double min, double max) {
    return (value - min) / (max - min) * 280;
}


void paintRegionWithSolutionValue(double resolution) {
    vector<double> valuesInPoints = {};
    for (const auto& p: pointListToDraw) {
        valuesInPoints.emplace_back((*solvedProblem)(p.x, p.y));
    }
    auto minValueIt = std::min_element(valuesInPoints.begin(), valuesInPoints.end());
    auto maxValueIt = std::max_element(valuesInPoints.begin(), valuesInPoints.end());
    //    Triangulation
    for(auto& t: triangleListFrame) {
        glBegin (GL_TRIANGLES);
        auto color1 = hslToRGB(functionFrom0To360(((*solvedProblem)(pointListToDraw[t.p1i].x, pointListToDraw[t.p1i].y)), (*minValueIt), (*maxValueIt)), 1, 0.5);
        glColor3ub(color1.red, color1.green, color1.blue);
        glVertex2f(pointListToDrawFrame[t.p1i].x, pointListToDrawFrame[t.p1i].y);
        auto color2 = hslToRGB(functionFrom0To360(((*solvedProblem)(pointListToDraw[t.p2i].x, pointListToDraw[t.p2i].y)), (*minValueIt), (*maxValueIt)), 1, 0.5);
        glColor3ub(color2.red, color2.green, color2.blue);
        glVertex2f(pointListToDrawFrame[t.p2i].x, pointListToDrawFrame[t.p2i].y);
        auto color3 = hslToRGB(functionFrom0To360(((*solvedProblem)(pointListToDraw[t.p3i].x, pointListToDraw[t.p3i].y)), (*minValueIt), (*maxValueIt)), 1, 0.5);
        glColor3ub(color3.red, color3.green, color3.blue);
        glVertex2f(pointListToDrawFrame[t.p3i].x, pointListToDrawFrame[t.p3i].y);
        glEnd();
    }
    vector<Point> drawNodes = fillWithNodes(regionPointsListFrame, resolution, resolution);
}

void paintRegionWithSolutionValueOpt(double resolution) {
    //    Triangulation
    for(auto& t: triangleListFrame) {
        glBegin (GL_TRIANGLES);
        glColor3ub(valuesInDrawPoints[t.p1i].red, valuesInDrawPoints[t.p1i].green, valuesInDrawPoints[t.p1i].blue);
        glVertex2f(pointListToDrawFrame[t.p1i].x, pointListToDrawFrame[t.p1i].y);
        glColor3ub(valuesInDrawPoints[t.p2i].red, valuesInDrawPoints[t.p2i].green, valuesInDrawPoints[t.p2i].blue);
        glVertex2f(pointListToDrawFrame[t.p2i].x, pointListToDrawFrame[t.p2i].y);
        glColor3ub(valuesInDrawPoints[t.p3i].red, valuesInDrawPoints[t.p3i].green, valuesInDrawPoints[t.p3i].blue);
        glVertex2f(pointListToDrawFrame[t.p3i].x, pointListToDrawFrame[t.p3i].y);
        glEnd();
    }
//    vector<Point> drawNodes = fillWithNodes(regionPointsListFrame, resolution, resolution);
}


ColorRGB hslToRGB(double h, double s, double l) {
    double c = (1 - abs(2 * l - 1)) * s;
    double x = c * (1 - std::abs((std::fmod((h / 60), 2)  - 1)));
    double m = l - c/2;
    double rs = 0, gs = 0, bs = 0;
    if (0 <= h && h < 60) {
        rs = c;
        gs = x;
        bs = 0;
    }
    if (60 <= h && h < 120) {
        rs = x;
        gs = c;
        bs = 0;
    }
    if (120 <= h && h < 180) {
        rs = 0;
        gs = c;
        bs = x;
    }
    if (180 <= h && h < 240) {
        rs = 0;
        gs = x;
        bs = c;
    }
    if (240 <= h && h < 300) {
        rs = x;
        gs = 0;
        bs = c;
    }
    if (300 <= h && h < 360) {
        rs = c;
        gs = 0;
        bs = x;
    }
    ColorRGB res((rs + m) * 255.0, (gs + m) * 255.0, (bs + m) * 255.0);
    return res;
}

Point realPointToDisplay(const Point& p) {
    return Point((p.x - frameCenter.x) * scale, (p.y - frameCenter.y) * scale);
}

Point displayPointToReal(const Point& p) {
    return Point(p.x / scale + frameCenter.x, p.y / scale + frameCenter.y);
}


